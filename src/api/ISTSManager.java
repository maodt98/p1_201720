package api;

import model.data_structures.IList;
import model.data_structures.LinkedList;
import model.data_structures.MyInteger;
import model.data_structures.Queue;
import model.vo.RoutesVO;
import model.vo.ServiceVO;
import model.vo.Stop_timesVO;
import model.vo.StopsVO;
import model.vo.TripsVO;

public interface ISTSManager {

	public void ITScargarGTFS( );
	
	public void ITScargarTR(String pFecha);
	//-------------------------PARTE A---------------------------//
LinkedList<RoutesVO> ITSrutasPorEmpresa(Integer fecha, String nombreEmpresa) throws Exception;
	
	LinkedList<TripsVO> ITSviajesRetrasadosRuta(Integer idRuta, Integer fecha) throws Exception;
	
	LinkedList<StopsVO> ITSparadasRetrasadasFecha( Integer fecha) throws Exception;
	
	LinkedList<Stop_timesVO> ITStransbordosRuta(Integer fecha,  Integer idRuta)throws Exception;
	
	LinkedList<TripsVO> ITSrutasPlanUtilizacion( String horaFin, String
	horaInicio, String fecha, String[] lista)throws Exception;
	
	//-------------------------PARTE B--------------------------//
	LinkedList<RoutesVO>ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha)throws Exception;
	
	LinkedList<TripsVO> ITSviajesRetrasoTotalRuta(String idRuta, String fecha)throws Exception;
	
	LinkedList<String> ITSretardoHoraRuta (String idRuta, String fecha )throws Exception;
	
	LinkedList<TripsVO> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaIn, String horaFin);
	
	RoutesVO ITSrutaMenorRetardo(String fecha)throws Exception;
	
	//-------------------------PARTE C-------------------------//
	LinkedList<ServiceVO> ITSserviciosMayorDistancia (String fecha);
	
	LinkedList<Integer> ITSretardosTrip(Integer nFecha, Integer nId);
	
	IList<Integer> ITSparadasCompartidas(String nFecha);
	
}
