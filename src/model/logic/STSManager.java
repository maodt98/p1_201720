package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ISTSManager;
import controller.Controller;
import model.data_structures.IList;
import model.data_structures.LinkedList;
import model.data_structures.MyInteger;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.RadixSort;
import model.data_structures.Stack;
import model.vo.AgencyVO;
import model.vo.Bus_ServiceVO;
import model.vo.CalendarVO;
import model.vo.Calendar_datesVO;
import model.vo.Feed_infoVO;
import model.vo.RoutesVO;
import model.vo.ScheduleVO;
import model.vo.ServiceVO;
import model.vo.ShapesVO;
import model.vo.Stop_timesVO;
import model.vo.StopsVO;
import model.vo.Stops_Estim_ServiceVO;
import model.vo.TransfersVO;
import model.vo.TripsVO;

public class STSManager<T> implements ISTSManager {

	private RadixSort radix;

	private LinkedList<AgencyVO> agency = new LinkedList<>();
	private LinkedList<Calendar_datesVO> calendarDates = new LinkedList<>();
	private LinkedList<CalendarVO> calendar = new LinkedList<>();
	private LinkedList<Feed_infoVO> feedInfo = new LinkedList<>();
	private LinkedList<RoutesVO> routes = new LinkedList<>();
	private LinkedList<ShapesVO> shapes = new LinkedList<>();
	private LinkedList<Stop_timesVO> stopTimes = new LinkedList<>();
	private LinkedList<StopsVO> stops = new LinkedList<>();
	private LinkedList<TransfersVO> transfers = new LinkedList<>();
	private LinkedList<TripsVO> trips = new LinkedList<>();
	private LinkedList<Bus_ServiceVO> busesService = new LinkedList<>();
	private LinkedList<Stops_Estim_ServiceVO> stopsEServ = new LinkedList<>();

	@Override
	public void ITScargarGTFS() {

		loadAgency();
		System.out.println("------------------CargÃ³ efectivamente Agency-------------------");
		loadCalendar_dates();
		System.out.println("---------------CargÃ³ efectivamente Calendar_dates--------------");
		loadCalendar();
		System.out.println("-----------------CargÃ³ efectivamente Calendar------------------");
		loadFeed();
		System.out.println("-------------------CargÃ³ efectivamente Feed--------------------");
		loadRoutes();
		System.out.println("------------------CargÃ³ efectivamente Routes-------------------");
		loadShapes();
		System.out.println("------------------CargÃ³ efectivamente Shapes-------------------");
		loadStopTimes();
		System.out.println("-----------------CargÃ³ efectivamente StopTimes-----------------");
		loadStops();
		System.out.println("-------------------CargÃ³ efectivamente Stops-------------------");
		loadTrips();
		System.out.println("-------------------CargÃ³ efectivamente Trips-------------------");
		loadTransfers();
		System.out.println("-----------------CargÃ³ efectivamente Transfers-----------------");
	}

	@Override
	public void ITScargarTR(String pFecha) {

		loadBusesService(pFecha);
		System.out.println("-------------------CargÃ³ efectivamente BusesService--------------------");
		loadStopsEstimService(pFecha);
		System.out.println("-----------------CargÃ³ efectivamente StopsEstimService-----------------");

	} 

	public void loadAgency() {
		String file = "./data/agency.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				AgencyVO x = new AgencyVO(datos[0], datos[1], datos[2], datos[3], datos[4]);
				agency.add(x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void loadCalendar_dates() {
		String file = "./data/calendar_dates.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				Calendar_datesVO x = new Calendar_datesVO(Integer.parseInt(datos[0]), Integer.parseInt(datos[1]), Byte.parseByte(datos[2]));
				calendarDates.add(x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadCalendar() {
		String file = "./data/calendar.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				CalendarVO x = new CalendarVO(Integer.parseInt(datos[0]), Byte.parseByte(datos[1]), Byte.parseByte(datos[2]), Byte.parseByte(datos[3]), Byte.parseByte(datos[4]), Byte.parseByte(datos[5]), Byte.parseByte(datos[6]), Byte.parseByte(datos[7]), Integer.parseInt(datos[8]), Integer.parseInt(datos[9]));
				calendar.add(x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadFeed() {
		String file = "./data/feed_info.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				Feed_infoVO x = new Feed_infoVO(datos[0], datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), datos[5]);
				feedInfo.add(x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadRoutes() {
		String file = "./data/routes.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			int i = 0;
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				RoutesVO x = new RoutesVO(Integer.parseInt(datos[0]), datos[1], datos[2], datos[3], datos[4], Byte.parseByte(datos[5]), datos[6], datos[7], datos[8]);
				routes.add(x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void loadShapes() {
		String file = "./data/shapes.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				ShapesVO x = new ShapesVO(Integer.parseInt(datos[0]), Double.parseDouble(datos[1]), Double.parseDouble(datos[2]), Short.parseShort(datos[3]), Double.parseDouble(datos[4]));
				shapes.add(x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadStopTimes() {
		String file = "./data/stop_times.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				Stop_timesVO x = new Stop_timesVO(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[3]), Integer.parseInt(datos[4]), datos[5], Byte.parseByte(datos[6]), Byte.parseByte(datos[7]), (datos.length>8)?Double.parseDouble(datos[8]):0);
				stopTimes.add(x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadStops() {
		String file = "./data/stops.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				boolean si = false;
				String[] datos = line.split(splitBy);
				StopsVO x = new StopsVO((!datos[0].isEmpty())?Integer.parseInt(datos[0]):0, (!(datos[1].equals(" ")))?Integer.parseInt(datos[1]):-1 , datos[2], datos[3], Double.parseDouble(datos[4]), Double.parseDouble(datos[5]), datos[6], datos[7], (!datos[8].isEmpty())?Byte.parseByte(datos[8]):0, (datos.length>9)?datos[9]:"");
				if(x.getStop_code() != -1){
					stops.add(x);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadTrips() {
		String file = "./data/trips.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				TripsVO x = new TripsVO(Integer.parseInt(datos[0]), Integer.parseInt(datos[1]), Integer.parseInt(datos[2]), datos[3], datos[4], Byte.parseByte(datos[5]), Integer.parseInt(datos[6]), Integer.parseInt(datos[7]), Byte.parseByte(datos[8]), Byte.parseByte(datos[9]));
				trips.add(x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadTransfers() {
		String file = "./data/transfers.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				TransfersVO x = new TransfersVO(Integer.parseInt(datos[0]), Integer.parseInt(datos[1]), Byte.parseByte(datos[2]), (datos.length>3)?Short.parseShort(datos[3]):0);
				transfers.add(x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadBusesService(String pFecha){
		File f;
		if(pFecha.equals("20170821")){
			f = new File("Buses_Service/RealTime-8-21-BUSES_SERVICE");
		}
		else if(pFecha.equals("20170822")){
			f = new File("Buses_Service/RealTime-8-22-BUSES_SERVICE");
		}
		else{
			System.out.println("No existen Buses Services con la fecha dada");
			return;
		}
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			readBusesService(updateFiles[i]);
		}
		//PRUEBA
		/**Node<Bus_ServiceVO> act = busesService.getHead();
		while(act != null) {
			System.out.println(act.getData().getDestination());
			act = act.getNext();
		}**/
	}

	public void readBusesService(File f){
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(f));
			Gson gson = new GsonBuilder().create();

			Bus_ServiceVO[] busesServicex = gson.fromJson(reader, Bus_ServiceVO[].class);

			for(int i=0; i<busesServicex.length; i++){
				busesService.add(busesServicex[i]);
			}

		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadStopsEstimService(String pFecha){
		File f;
		if(pFecha.equals("20170821")){
			f = new File("Stop_Stim_Service/RealTime-8-21-STOPS_ESTIM_SERVICES");
		}
		else if(pFecha.equals("20170822")){
			f = new File("Stop_Stim_Service/RealTime-8-22-STOPS_ESTIM_SERVICES");
		}
		else{
			System.out.println("No existe el Stop Estim Service con la fecha dada");
			return;
		}
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			readStopsEstimService(updateFiles[i]);
		}
		//PRUEBA
		/**Node<Stops_Estim_ServiceVO> act = stopsEServ.getHead();
		while(act != null) {
			System.out.println(act.getData().getRouteName());
			act = act.getNext();
		}**/
	}

	public void readStopsEstimService(File f){
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(f));
			String linea = reader.readLine();

			if(linea.startsWith("{\"Code\"")){
				return;
			}
			reader = new BufferedReader(new FileReader(f));
			Gson gson = new GsonBuilder().create();
			Stops_Estim_ServiceVO[] stopsEServx = gson.fromJson(reader, Stops_Estim_ServiceVO[].class);
			String[] partes = f.getName().split("_");
			String[] partes2 = partes[3].split("-");

			for(int i=0; i<stopsEServx.length; i++){
				stopsEServx[i].setStop_code(Integer.parseInt(partes2[0]));
				stopsEServ.add(stopsEServx[i]);
			}

		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}catch(IOException ex){
			ex.printStackTrace();	
		}finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	//--------------------------------------------------------------------------------------
	//----------------------------------------PARTE JOSEPH---------------------------------
	/**
	 * Coge la lista dada por parámetro y la convierte en un arreglo MyInteger
	 * @param lista Lista que se quiere convertir
	 * @return Arreglo MyInteger con la lista dada por parámetro
	 */
	public MyInteger<T>[] agregarListaArreglo(LinkedList pLista)
	{
		MyInteger<T>[] arr = new MyInteger[pLista.getSize()];
		Node<T> act = pLista.getHead();

		for(int i = 0; i<pLista.getSize()&& act != null; i++){
			MyInteger<T> n = new MyInteger<T>(i);
			n.setData(act.getData());
			arr[i] = n;
			act = act.getNext();}
		return arr;
	}
	/**
	 * Coge una lista de MyInteger y la pasa a un arreglo
	 * @param lista Lista de MyInteger que se quiere convertir
	 * @return Arreglo MyInteger con la lista
	 */
	public MyInteger[] agregarListaMyIntArreglo(LinkedList lista)
	{
		MyInteger[] retornar = new MyInteger[lista.getSize()];
		Node<T> actual = lista.getHead();
		T obj = actual.getData();
		while(actual.getNext()!=null)
		{
			for(int i = 0; i<retornar.length;i++)
			{
				retornar[i].setData(obj);

			}
			actual = actual.getNext();
		}
		return retornar;
	}
	/**
	 * Coge una Stack de MyInteger y la pasa a un arreglo
	 * @param lista Lista de MyInteger que se quiere convertir
	 * @return Arreglo MyInteger con la lista
	 * @throws Exception 
	 */
	public MyInteger[] agregarPilaMyIntArreglo(Stack<MyInteger> lista) throws Exception
	{
		MyInteger[] retornar = new MyInteger[lista.getSize()];
		Node<MyInteger> actual = lista.pop();
		MyInteger obj = actual.getData();
		while(actual.getNext()!=null)
		{
			for(int i = 0; i<retornar.length;i++)
			{
				retornar[i].setData(obj);
				retornar[i].setValue(obj.getValue());
			}
			actual = lista.pop();
		}
		return retornar;
	}

	//-----------------------------------------------------------------------------------------------------------
	// PUNTO 1A
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Retorna una lista con el ID de los servicios pertenecientes a la fecha dada por parámetro
	 * @param nFecha Fecha de la que se desean saber los servicios
	 * @return Lista con el id de los servicios que pertenecen a la fecha dada.
	 */
	private LinkedList darIDServicios(Integer nFecha)
	{
		LinkedList<Integer> servicios = new LinkedList<Integer>();
		Node<CalendarVO> currentDate = calendar.getHead();

		Integer serv1=0,serv2=0;

		while(currentDate!=null)
		{
			CalendarVO objDate = currentDate.getData();
			if(nFecha >= objDate.getStart_date() && nFecha <= objDate.getEnd_date())
			{
				serv1 = objDate.getService_id();
				servicios.add(serv1);
			}

			currentDate = currentDate.getNext();
		}
		return servicios;
	}

	/**
	 * Ordena los servicios segun su ID
	 * @param nFecha Fecha de los servicios que se necesitan
	 * @return Lista ordenada con el ID de los servicios
	 */
	private MyInteger<Integer>[] ordenarServicios(Integer nFecha)
	{

		MyInteger<Integer>[] arrNuevaL = (MyInteger<Integer>[]) listaArreglo(darIDServicios(nFecha));

		for(int i = 0; i <arrNuevaL.length;i++)
		{
			Integer num = arrNuevaL[i].getData();
			arrNuevaL[i].setValue(num);
		}
		arrNuevaL = radix.sort(arrNuevaL);
		return arrNuevaL;


	}
	/**
	 * Cambia el nombre de la empresa con el ID por el que se va a buscar
	 * @param nombreEmpresa
	 * @return
	 */
	private String darIDEmpresa(String nombreEmpresa) 
	{
		String retornar = null;

		if(nombreEmpresa.equalsIgnoreCase("TransLink")) {
			retornar = "CMBC";

		}
		else if(nombreEmpresa.equalsIgnoreCase("West Coast Express")) {
			retornar = "WCEX";
		}
		else if(nombreEmpresa.equalsIgnoreCase("British Columbia Rapid Transit Company")) {
			retornar = "SKYT";
		}

		return retornar;
	}
	/**
	 * Retorna todas las rutas que pertenecen a la empresa con el nombre dado por parámetro
	 * @param agency Nombre de la empresa
	 * @return Rutas de la empresa
	 * @throws Exception 
	 */
	private LinkedList<RoutesVO> darRutasNombre(String agency) 
	{
		Node<RoutesVO> currentRoute = routes.getHead();
		LinkedList<RoutesVO> retornar = new LinkedList<RoutesVO>();
		while(currentRoute!=null)
		{
			RoutesVO objRuta = currentRoute.getData();
			if(objRuta.getAgency_id().equals(darIDEmpresa(agency)))
			{
				retornar.add(objRuta);
			}
			currentRoute = currentRoute.getNext();
		}


		return retornar;
	}
	/**
	 * Ordena las rutas, según su ID
	 * @param nFecha Fecha de los servicios que se necesitan
	 * @return Lista ordenada con el ID de los servicios
	 */
	private MyInteger<RoutesVO>[] ordenarRutasNombre(String agency)
	{
		LinkedList<RoutesVO> rutas = darRutasNombre(agency);
		MyInteger<RoutesVO>[] arr = (MyInteger<RoutesVO>[])agregarListaArreglo(rutas);
		for(int i = 0; i< arr.length;i++)
		{
			RoutesVO ruta = arr[i].getData();
			arr[i].setValue(ruta.getRoute_id());
		}
		arr = radix.sort(arr) ;
		return arr;
	}
	/**
	 * Ordena la lista de rutas según su ID
	 * @return Arreglo de rutas ordenadas por ID
	 */
	private MyInteger<RoutesVO>[] ordenarRutas()
	{
		MyInteger<RoutesVO>[] arr = (MyInteger<RoutesVO>[])agregarListaArreglo(routes);



		for(int i = 0; i< arr.length;i++)
		{
			RoutesVO ruta = arr[i].getData();
			arr[i].setValue(ruta.getRoute_id());
		}
		arr= radix.sort(arr);
		return arr;
	}
	/**
	 * Busca y retorna una ruta según su ID
	 * @param ID identificador de la ruta que se quiere encontrar
	 * @return Ruta encontrada
	 */
	public RoutesVO darRutaID(Integer ID)
	{
		RoutesVO retornar = null;
		MyInteger<RoutesVO>[] ordRutas = ordenarRutas();
		int n = ordRutas.length;
		Integer cen = 0 , low = 0, high = n-1;
		while(low<=high)
		{ 
			cen = (low+high)/2;

			RoutesVO objRuta = ordRutas[cen].getData();
			if (ID.intValue() == objRuta.getRoute_id())
			{
				retornar = objRuta;
			}

			else if(ID.intValue() < ordRutas[cen].getData().getRoute_id() ){
				high=cen-1;
			}
			else {
				low=cen+1;
			}
		}
		System.out.println(retornar);
		return retornar;
	}



	/**
	 * Retorna una lista con las rutas ordenadas por su id, buscando aquellos que sean de una empresa dada, en una fecha dada.
	 * @param nEmpresa Empresa de la que se desea saber la ruta.
	 * @param nFecha Fecha de la que se quiere saber las rutas
	 * @return Lista de rutas ordenadas por su ID.
	 * @throws Exception 
	 */
	public LinkedList<RoutesVO> ITSrutasPorEmpresa(Integer fecha, String nombreEmpresa) throws Exception
	{
		String[] partes = nombreEmpresa.split(" ");
		if(nombreEmpresa.equalsIgnoreCase("TransLink")) {
			nombreEmpresa = "CMBC";
		}
		else if(nombreEmpresa.equalsIgnoreCase("West Coast Express")) {
			nombreEmpresa = "WCEX";
		}
		else if(nombreEmpresa.equalsIgnoreCase("British Columbia Rapid Transit Company")) {
			nombreEmpresa = "SKYT";
		}
		else {
			throw new Exception("No existe ninguna empresa con el nombre dado");}
		//

		LinkedList<CalendarVO> serviceIds = new LinkedList<CalendarVO>();
		Node<CalendarVO> cActual = calendar.getHead();
		while(cActual != null){
			CalendarVO  act = cActual.getData();
			int fechAct = fecha;
			if((act.getStart_date()<=fechAct) && (fechAct<=act.getEnd_date())){
				serviceIds.add(act);
			}
			cActual = cActual.getNext();
		}


		if(serviceIds.getSize() == 0) {
			throw new Exception("No se encontraron rutas con la fecha dada");
		}

		LinkedList<Integer> rutasIds = new LinkedList<Integer>();
		Node<TripsVO> tActual = trips.getHead();
		while(tActual != null){
			TripsVO act = tActual.getData();
			Node<CalendarVO> iAct = serviceIds.getHead();
			while(iAct != null){
				if(act.getService_id() == iAct.getData().getService_id()){
					rutasIds.add(act.getRoute_id());
				}
				iAct = iAct.getNext();
			}
			tActual = tActual.getNext();
		}


		LinkedList<Integer> rutasIdsOrd = new LinkedList<Integer>();
		MyInteger<Integer>[] arrNuevaL = (MyInteger<Integer>[]) listaArreglo(rutasIds);
		arrNuevaL = radix.sort(arrNuevaL);
		MyInteger<Integer> intAct = arrNuevaL[0]; 
		rutasIdsOrd.add(arrNuevaL[0].getData());
		for(int i=0; i<arrNuevaL.length; i++){
			MyInteger<Integer> actual = arrNuevaL[i];
			if(intAct.getData().intValue() != actual.getData().intValue()){
				rutasIdsOrd.add(actual.getData());
				intAct = actual;
			}
		}


		if(rutasIds.getSize() == 0) {
			throw new Exception("No se encontraron rutas con la fecha dada");
		}

		LinkedList<RoutesVO> rutas = new LinkedList<RoutesVO>();
		Node<Integer> iAct = rutasIdsOrd.getHead();
		while(iAct!=null){
			Node<RoutesVO> rAct = routes.getHead();
			while(rAct != null){
				if(iAct.getData().intValue() == rAct.getData().getRoute_id() && rAct.getData().getAgency_id().equals(nombreEmpresa)){
					rutas.add(rAct.getData());
				}
				rAct = rAct.getNext();
			}
			iAct = iAct.getNext();
		}

		if(rutas.getSize() == 0) {
			throw new Exception("No se encontraron rutas con la empresa dada");
		}
		// Ac� se Ordena

		MyInteger<RoutesVO>[] ordenar = (MyInteger<RoutesVO>[]) agregarListaArreglo(rutas);
		System.out.println(ordenar.length);
		for(int i = 0; i<ordenar.length;i++)
		{
			RoutesVO act = ordenar[i].getData();
			ordenar[i].setValue(act.getRoute_id());
		}
		ordenar = radix.sort(ordenar);
		LinkedList<RoutesVO> retornar = new LinkedList<RoutesVO>();

		for(int i = 0; i<ordenar.length;i++)
		{
			retornar.addAtEnd(ordenar[i].getData());
		}
		LinkedList<RoutesVO> routesOrd = new LinkedList<RoutesVO>();
		MyInteger<RoutesVO>[] arrNueva = ordenar;
		MyInteger<RoutesVO> RouteAct = arrNueva[0]; 
		routesOrd.add(arrNueva[0].getData());
		for(int i=0; i<arrNueva.length; i++){
			MyInteger<RoutesVO> actual = arrNueva[i];
			if(RouteAct.getData() != actual.getData()){
				routesOrd.addAtEnd(actual.getData());
				RouteAct = actual;
			}
		}



		return routesOrd;

	}


	//-----------------------------------------------------------------------------------------------------------
	// PUNTO 2A
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Retorna una lista con todos los viajes que se aplican en una fecha y una ruta dada por parámetro
	 * @param nFecha 
	 * @param idRuta
	 * @return
	 */
	private LinkedList<TripsVO> darTripsRutaServicio(Integer nFecha, Integer idRuta)
	{
		LinkedList<TripsVO> retornar = new LinkedList<TripsVO>();
		//Servicios
		LinkedList<Integer> servicios = darIDServicios(nFecha);
		Node<Integer> currentService = servicios.getHead();

		RoutesVO ruta = darRutaID(idRuta);
		// Trips
		Node<TripsVO> currentTrip= trips.getHead();

		while(currentService!=null)
		{	
			Integer obj = currentService.getData();
			while(currentTrip!=null)
			{	
				TripsVO objTrip = currentTrip.getData();
				TripsVO tripAgregar =objTrip.darTripRutaServicio(ruta.getRoute_id(), obj);
				if(tripAgregar!=null)
				{
					retornar.add(tripAgregar);
				}
				currentTrip = currentTrip.getNext();
			}
			currentService = currentService.getNext();
		}

		return retornar;

	}
	/**
	 * Ordena los trips encontrados por Ruta y fecha, según su ID
	 * @param nFecha Fecha de la que se quieren saber los trips
	 * @param idRuta Ruta de la que se quieren saber los trips
	 * @return Arreglo ordenado con los trips encontrados
	 */
	private MyInteger<TripsVO>[] ordenarTripsRutaServicio(Integer nFecha, Integer idRuta)
	{
		LinkedList<TripsVO> trips = darTripsRutaServicio(nFecha, idRuta);
		MyInteger<TripsVO>[] arr = (MyInteger<TripsVO>[])agregarListaArreglo(trips);
		for(int i = 0; i< arr.length;i++)
		{
			TripsVO trip = arr[i].getData();
			arr[i].setValue(trip.getTrip_id());
		}
		arr = radix.sort(arr);
		return arr;
	}




	/**
	 * Ordena los stops según su tripID
	 * @return Lista ordenada con los stops según su TripID
	 */
	private MyInteger<Stop_timesVO>[] ordenarStopTimes(){

		MyInteger<Stop_timesVO>[] arrNuevaL = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);

		for(int i = 0; i <arrNuevaL.length;i++)
		{
			Stop_timesVO stop = arrNuevaL[i].getData();
			arrNuevaL[i].setValue(stop.getTrip_id());
		}
		arrNuevaL = radix.sort(arrNuevaL);
		arrNuevaL = radix.sort(arrNuevaL);

		return arrNuevaL;

	}
	/**
	 * Realiza búsqueda binaria entre los Stop_times y los trips, para encontrar los Stops del viaje
	 * @param nFecha Fecha de la que se quieren saber los trips
	 * @param idRuta Ruta de la que se quieren saber los trips
	 * @return Lista con los Stop_times del viaje
	 */
	private LinkedList<Stop_timesVO> darStopTimesFechaRuta(Integer nFecha, Integer idRuta)
	{	
		MyInteger<Stop_timesVO>[] ordStops = ordenarStopTimes();

		LinkedList<TripsVO> lista = darTripsRutaServicio(nFecha, idRuta);
		Node<TripsVO> currentTrip = lista.getHead();


		LinkedList<Stop_timesVO> retornar = new LinkedList<Stop_timesVO>();

		LinkedList<Stop_timesVO> stopsIds = new LinkedList<Stop_timesVO>();
		while(currentTrip != null){
			int n = ordStops.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(ordStops[centro].getData().getTrip_id().intValue()==currentTrip.getData().getTrip_id().intValue()){
					stopsIds.add(ordStops[centro].getData());
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<ordStops.length && ordStops[mayor].getData() != null  && !s1){
						if(ordStops[mayor].getData().getTrip_id() == currentTrip.getData().getTrip_id()){
							stopsIds.add(ordStops[mayor].getData());
							mayor += 1;
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && ordStops[menor].getData() != null && !s2){
						if(ordStops[menor].getData().getTrip_id() == currentTrip.getData().getTrip_id()){
							stopsIds.add(ordStops[menor].getData());
							menor -=1;
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(currentTrip.getData().getTrip_id() <ordStops[centro].getData().getTrip_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			currentTrip = currentTrip.getNext();
		}
		return stopsIds;
	}
	/**
	 * Ordena los stop times según su stopID
	 * @param nFecha
	 * @param idRuta
	 * @return
	 */
	private MyInteger<Stop_timesVO>[] ordenarStopTimesFechaRuta(Integer nFecha, Integer idRuta)
	{
		LinkedList<Stop_timesVO> stopTimes = darStopTimesFechaRuta(nFecha, idRuta);
		MyInteger<Stop_timesVO>[] retornar = (MyInteger<Stop_timesVO>[])agregarListaArreglo(stopTimes);

		for(int i = 0; i< retornar.length;i++)
		{
			Stop_timesVO curr = retornar[i].getData();
			retornar[i].setValue(curr.getStop_id());
		}
		retornar = radix.sort(retornar);
		return retornar;
	}
	/**
	 * Retorna todos los stops pertenecientes a los stop times en una fecha y una ruta
	 * @param nFecha Fecha de la que se quieren saber los stops
	 * @param idRuta Ruta de la que se quieren saber los stops
	 * @return Lista con los stops pertenecientes a la fecha y ruta
	 */
	private LinkedList<StopsVO> darStopsFechaRuta(Integer nFecha, Integer idRuta)
	{

		MyInteger<Stop_timesVO>[] ordenado = ordenarStopTimesFechaRuta(nFecha, idRuta);
		Node<StopsVO> currentStop = stops.getHead();

		LinkedList<StopsVO> retornar = new LinkedList<StopsVO>();

		while(currentStop != null){
			int n = ordenado.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(ordenado[centro].getData().getTrip_id().intValue()==currentStop.getData().getStop_id()){
					retornar.add(currentStop.getData());
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<ordenado.length && ordenado[mayor].getData() != null  && !s1){
						if(ordenado[mayor].getData().getStop_id() == currentStop.getData().getStop_id()){
							retornar.add(currentStop.getData());
							mayor += 1;
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && ordenado[menor].getData() != null && !s2){
						if(ordenado[menor].getData().getStop_id() == currentStop.getData().getStop_id()){

							retornar.add(currentStop.getData());
							menor -=1;
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(currentStop.getData().getStop_id() <ordenado[centro].getData().getStop_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			currentStop = currentStop.getNext();
		}
		return retornar;
	}
	/**
	 * Retorna una lista con todos los buses services dada una fecha y una ruta
	 * @param nFecha
	 * @param idRuta
	 * @return
	 */
	private LinkedList<Bus_ServiceVO> darBusesServicesFechaRuta(Integer nFecha, Integer idRuta)
	{
		MyInteger<TripsVO>[] ordenado = ordenarTripsRutaServicio(nFecha, idRuta);
		Node<Bus_ServiceVO> currentBus = busesService.getHead();

		LinkedList<Bus_ServiceVO> retornar = new LinkedList<Bus_ServiceVO>();

		while(currentBus!=null)
		{

			Bus_ServiceVO objBus = currentBus.getData();
			int n = ordenado.length;
			Integer cen = 0 , low = 0, high = n-1;
			while(low<=high)
			{ cen = (low+high)/2;

			TripsVO objTripsVO = ordenado[cen].getData();
			if (objTripsVO.getTrip_id()== objBus.getTripId())
			{
				retornar.add(objBus);
			}

			else if(objBus.getTripId() < ordenado[cen].getData().getTrip_id()){
				high=cen-1;
			}
			else {
				low=cen+1;
			}


			}
			currentBus = currentBus.getNext();
		}
		return retornar;
	}


	/**
	 * Retorna una lista con todas las paradas que tienen retardo en una fecha dada
	 * @param nFecha
	 * @param idRuta
	 * @return
	 */
	private LinkedList<MyInteger> darRetardosFechaRuta(Integer nFecha, Integer idRuta)
	{
		LinkedList<MyInteger> retornar = new LinkedList<MyInteger>();
		LinkedList<Bus_ServiceVO> busesServices = darBusesServicesFechaRuta(nFecha, idRuta);
		Integer retardos = 0;
		Node<Bus_ServiceVO> currentService = busesService.getHead();

		LinkedList<Stop_timesVO> stops = darStopTimesFechaRuta(nFecha, idRuta); 
		Node<Stop_timesVO> currentStop = stops.getHead();
		while(currentStop!=null)
		{
			Stop_timesVO objStop = currentStop.getData();
			while(currentService!=null)
			{
				Bus_ServiceVO objServ = currentService.getData();
				if(objStop.getTrip_id()==objServ.getTripId())
				{
					if(objServ.getRecordedTime().compareTo(objStop.getArrival_time())>0)
					{
						Node<MyInteger> intCurr = retornar.getHead();
						while(intCurr!=null)
						{
							MyInteger inte = intCurr.getData();
							if(inte.getData()==objStop)
							{
								Integer ant = inte.getValue();
								inte.setValue(ant++);
							}
							else
							{
								MyInteger<TripsVO> agr = new MyInteger(1);
								agr.setData(darTripID(objStop.getTrip_id()));
								retornar.add(agr);
							}
							intCurr = intCurr.getNext();
						}
					}
				}
				currentService = currentService.getNext();
			}
			currentStop = currentStop.getNext();
		}
		return retornar;
	}
	/**
	 * Retorna una lista de las rutas que tienen retardo dada una ruta y una fecha, ordenadas por el TripID
	 * @param idRuta Ruta de la que se quieren saber las paradas
	 * @param nFecha Fecha de la que se quieren saber las paradas
	 * @return Lista ordenada con las stopTimes ordenadas por tripID
	 * @throws Exception 
	 */
	public LinkedList<TripsVO> ITSviajesRetrasadosRuta(Integer idRuta, Integer nFecha) throws Exception
	{
		Integer fecha = nFecha;
		if(fecha != 20170821 && fecha != 20170822){
			throw new Exception("No hay datos para le fecha ingresada");
		}

		//Trips en una fecha y una ruta dada
		LinkedList<TripsVO> listaTrips = tripsEnFecha(idRuta, fecha);	
		//Ordena los stop times
		MyInteger<Stop_timesVO>[] arrStopTimes = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);

		Node<TripsVO> tAct = listaTrips.getHead();
		for(int i = 0; i<arrStopTimes.length; i++){
			arrStopTimes[i].setValue(arrStopTimes[i].getData().getTrip_id());
		}
		//Saca todos los stop times de los trips
		arrStopTimes = radix.sort(arrStopTimes);

		LinkedList<Integer> stopsIds = new LinkedList<Integer>();

		while(tAct != null){
			int n = arrStopTimes.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrStopTimes[centro].getData().getTrip_id().intValue()==tAct.getData().getTrip_id().intValue()){
					stopsIds.add(arrStopTimes[centro].getData().getStop_id());
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<arrStopTimes.length && arrStopTimes[mayor].getData() != null  && !s1){
						if(arrStopTimes[mayor].getData().getTrip_id() == tAct.getData().getTrip_id()){
							stopsIds.add(arrStopTimes[mayor].getData().getStop_id());
							mayor += 1;
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && arrStopTimes[menor].getData() != null && !s2){
						if(arrStopTimes[menor].getData().getTrip_id() == tAct.getData().getTrip_id()){
							stopsIds.add(arrStopTimes[menor].getData().getStop_id());
							menor -=1;
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(tAct.getData().getTrip_id() < arrStopTimes[centro].getData().getTrip_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			tAct = tAct.getNext();
		}

		LinkedList<Integer> stopsIdsBien = new LinkedList<Integer>();
		Node<Integer> sAct = stopsIds.getHead();
		MyInteger<Integer>[] arrOrdStopsIds = (MyInteger<Integer>[]) listaArreglo(stopsIds);
		arrOrdStopsIds = radix.sort(arrOrdStopsIds);
		MyInteger<Integer> myIntAct = arrOrdStopsIds[0];
		stopsIdsBien.addAtEnd(myIntAct.getData());
		for(int i=0; i<arrOrdStopsIds.length; i++){
			MyInteger<Integer> actual = arrOrdStopsIds[i];
			if(myIntAct.getData().intValue() != actual.getData().intValue()){
				stopsIdsBien.addAtEnd(actual.getData());
				myIntAct = actual;
			}
		}


		MyInteger<StopsVO>[] arrOrdStops = (MyInteger<StopsVO>[]) listaArreglo(stops);
		for(int i = 0; i<arrOrdStops.length; i++){
			arrOrdStops[i].setValue(arrOrdStops[i].getData().getStop_id());
		}
		arrOrdStops = radix.sort(arrOrdStops);

		LinkedList<Integer> stopsCodes = new LinkedList<Integer>();
		Node<Integer> stopIdAct = stopsIdsBien.getHead();
		while(stopIdAct != null){
			int n = arrOrdStops.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStops[centro].getData().getStop_id()==stopIdAct.getData().intValue()){
					stopsCodes.add(arrOrdStops[centro].getData().getStop_code());
					break;
				}
				else if(stopIdAct.getData().intValue() < arrOrdStops[centro].getData().getStop_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			stopIdAct = stopIdAct.getNext();
		}

		/**
		 * Busca los stop estim service que tengan el mismo stop code que las paradas
		 */
		LinkedList<Stops_Estim_ServiceVO> stopEstServ = new LinkedList<Stops_Estim_ServiceVO>();
		MyInteger<Stops_Estim_ServiceVO>[] arrOrdStopsEstServ = (MyInteger<Stops_Estim_ServiceVO>[]) listaArreglo(stopsEServ);
		for(int i=0; i<arrOrdStopsEstServ.length; i++){
			arrOrdStopsEstServ[i].setValue(arrOrdStopsEstServ[i].getData().getStop_code());
		}
		arrOrdStopsEstServ = radix.sort(arrOrdStopsEstServ);
		System.out.println(4);
		Node<Integer> stopCodeAct = stopsCodes.getHead();
		while(stopCodeAct != null){
			int n = arrOrdStopsEstServ.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStopsEstServ[centro].getData().getStop_code().intValue()==stopCodeAct.getData().intValue()){
					stopEstServ.add(arrOrdStopsEstServ[centro].getData());
					break;
				}
				else if(stopCodeAct.getData().intValue() < arrOrdStopsEstServ[centro].getData().getStop_code().intValue() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			stopCodeAct = stopCodeAct.getNext();
		}


		LinkedList<Integer> stopEstServStopCodeRet = new LinkedList<Integer>();
		Node<Stops_Estim_ServiceVO> stESActual = stopEstServ.getHead();
		while(stESActual != null){
			Iterator<ScheduleVO> it = stESActual.getData().getSchedules().iterator();
			while(it.hasNext()){
				ScheduleVO scAct = it.next();
				if(scAct.getScheduleStatus().equals("-")){

					stopEstServStopCodeRet.add(stESActual.getData().getStop_code());

					break;
				}
			}
			stESActual = stESActual.getNext();
		}


		//Retorna los stop ids que se van a utilizar para retornar los trips al final
		LinkedList<Integer> stopsIdsDef = new LinkedList<Integer>();
		Node<Integer> intStopCodeAct = stopEstServStopCodeRet.getHead();
		while(intStopCodeAct != null){
			int n = arrOrdStops.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStops[centro].getData().getStop_code()==intStopCodeAct.getData().intValue()){
					stopsIdsDef.add(arrOrdStops[centro].getData().getStop_id());
					break;
				}
				else if(intStopCodeAct.getData().intValue() < arrOrdStops[centro].getData().getStop_code() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			intStopCodeAct = intStopCodeAct.getNext();
		}


		LinkedList<Integer> listaTripsIdsDef = new LinkedList<Integer>();
		Node<Integer> stopIdDefAct = stopsIdsDef.getHead();
		MyInteger<Stop_timesVO>[] arrOrdStopT = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);
		for(int i = 0; i<arrOrdStopT.length; i++){
			arrOrdStopT[i].setValue(arrOrdStopT[i].getData().getStop_id());
		}

		arrOrdStopT = radix.sort(arrOrdStopT);

		while(stopIdDefAct != null){
			int n = arrOrdStopT.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStopT[centro].getData().getStop_id()==stopIdDefAct.getData().intValue()){
					listaTripsIdsDef.add(arrOrdStopT[centro].getData().getTrip_id());
					break;
				}
				else if(stopIdDefAct.getData().intValue() < arrOrdStopT[centro].getData().getStop_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			stopIdDefAct = stopIdDefAct.getNext();
		}


		LinkedList<TripsVO> listatrips = new LinkedList<TripsVO>();
		Node<TripsVO> nodoAct = listatrips.getHead();
		LinkedList<Integer> listaTripsID = new LinkedList<Integer>();
		Node<Integer> tripIdDefAct = listaTripsIdsDef.getHead();
		MyInteger<TripsVO>[] arrOrdTrips = (MyInteger<TripsVO>[]) listaArreglo(trips);
		for(int i=0; i<arrOrdTrips.length; i++){
			arrOrdTrips[i].setValue(arrOrdTrips[i].getData().getTrip_id());
		}
		arrOrdTrips = radix.sort(arrOrdTrips);
		while(tripIdDefAct!= null){
			int n = arrOrdTrips.length;
			int centro,inf=0,sup=n-1;

			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdTrips[centro].getData().getTrip_id()==tripIdDefAct.getData().intValue()){
					listatrips.add(arrOrdTrips[centro].getData());
					listaTripsID.add(arrOrdTrips[centro].getData().getTrip_id());
					while(nodoAct!=null)
					{
						if( nodoAct.getData()==nodoAct.getNext().getData())
						{
							listatrips.delete(nodoAct.getNext());
						}
						nodoAct = nodoAct.getNext();
					}
					break;
				}
				else if(tripIdDefAct.getData().intValue() < arrOrdTrips[centro].getData().getTrip_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			tripIdDefAct = tripIdDefAct.getNext();
		}

		MyInteger<TripsVO>[] ordenar = (MyInteger<TripsVO>[]) listaArreglo(listatrips);
		for(int i = 0; i<ordenar.length;i++)
		{
			TripsVO trip = ordenar[i].getData();
			ordenar[i].setValue(trip.getTrip_id());
		}
		ordenar = radix.sort(ordenar);
		LinkedList<TripsVO> tripsOrd = new LinkedList<TripsVO>();
		MyInteger<TripsVO>[] arrNuevaL = ordenar;
		MyInteger<TripsVO> tripAct = arrNuevaL[0]; 
		tripsOrd.add(arrNuevaL[0].getData());
		for(int i=0; i<arrNuevaL.length; i++){
			MyInteger<TripsVO> actual = arrNuevaL[i];
			if(tripAct.getData() != actual.getData()){
				tripsOrd.addAtEnd(actual.getData());
				tripAct = actual;
			}
		}
		return tripsOrd;
	}


	//-----------------------------------------------------------------------------------------------------------
	// PUNTO 3A
	//-----------------------------------------------------------------------------------------------------------

	/**
	 * Ordena todos los trips según serviceID
	 * @return Arreglo con los trips ordenados por  serviceID
	 */
	private MyInteger<TripsVO>[] ordenarTrips()
	{
		MyInteger<TripsVO>[] arrNuevaL = (MyInteger<TripsVO>[]) listaArreglo(trips);

		for(int i = 0; i <arrNuevaL.length;i++)
		{
			TripsVO trip = arrNuevaL[i].getData();
			arrNuevaL[i].setValue(trip.getTrip_id());
		}
		arrNuevaL = radix.sort(arrNuevaL);
		System.out.print(arrNuevaL.length);
		return arrNuevaL;
	}
	/**
	 * Retorna una lista con todos los trips, dada una fecha por parámetro
	 * @param nFecha Fecha de la que se quieren saber los trips
	 * @return Lista con los trips de la fecha
	 */
	private LinkedList<TripsVO> darTripsServicios(Integer nFecha)
	{

		MyInteger<TripsVO>[] ordTrips = ordenarTrips();

		LinkedList<Integer> lista = darIDServicios(nFecha) ;
		Node<Integer> currentService = lista.getHead();


		LinkedList<TripsVO> retornar = new LinkedList<TripsVO>();

		while(currentService!=null)
		{

			Integer objServ = currentService.getData();
			int n = ordTrips.length;
			Integer cen = 0 , low = 0, high = n-1;
			while(low<=high)
			{ cen = (low+high)/2;

			TripsVO objTrips = ordTrips[cen].getData();
			if (objTrips.getService_id()==objServ)
			{
				retornar.add(objTrips);
			}

			else if(objServ < ordTrips[cen].getData().getTrip_id() ){
				high=cen-1;
			}
			else {
				low=cen+1;
			}


			}
			currentService= currentService.getNext();
		}
		return retornar;
	}
	/**
	 * Ordena los trips de una fecha dada, según su ID
	 * @param nFecha fecha de la que se quieren saber los trips
	 * @return Lista ordenada con los trips según su ID
	 */
	private MyInteger<TripsVO>[] ordenarTripsFechaID(Integer nFecha)
	{
		MyInteger<TripsVO>[] ordenar = (MyInteger<TripsVO>[])agregarListaArreglo(darTripsServicios(nFecha));
		for(int i = 0; i< ordenar.length;i++)
		{
			TripsVO trip = ordenar[i].getData();
			ordenar[i].setValue(trip.getTrip_id());
		}
		ordenar = radix.sort(ordenar);
		return ordenar;
	}
	/**
	 * Realiza búsqueda binaria entre los Stop_times y los trips, para encontrar los Stops del viaje
	 * @param nFecha Fecha de la que se quieren saber los trips
	 * @param idRuta Ruta de la que se quieren saber los trips
	 * @return Lista con los Stop_times del viaje
	 */
	private LinkedList<Stop_timesVO> darStopTimesFecha(Integer nFecha)
	{	
		MyInteger<Stop_timesVO>[] ordStops = ordenarStopTimes();

		LinkedList<TripsVO> lista = darTripsServicios(nFecha);
		Node<TripsVO> currentTrip = lista.getHead();


		LinkedList<Stop_timesVO> retornar = null;

		while(currentTrip!=null)
		{

			TripsVO objTrip = currentTrip.getData();
			int n = ordStops.length;
			Integer cen = 0 , low = 0, high = n-1;
			while(low<=high)
			{ cen = (low+high)/2;

			Stop_timesVO objStop = ordStops[cen].getData();
			if (objTrip.getTrip_id()== objStop.getTrip_id())
			{
				retornar.add(objStop);
			}

			else if(objTrip.getTrip_id() < ordStops[cen].getData().getTrip_id() ){
				high=cen-1;
			}
			else {
				low=cen+1;
			}

			currentTrip = currentTrip.getNext();
			}
		}
		return retornar;
	}
	private MyInteger<Bus_ServiceVO>[] ordenarBusesServiceTripID()
	{
		MyInteger<Bus_ServiceVO>[] arrNuevaL = (MyInteger<Bus_ServiceVO>[]) listaArreglo(busesService);

		for(int i = 0; i <arrNuevaL.length;i++)
		{
			Bus_ServiceVO bus = arrNuevaL[i].getData();
			arrNuevaL[i].setValue(bus.getTripId());
		}
		arrNuevaL = radix.sort(arrNuevaL);


		return arrNuevaL;
	}
	/**
	 * Retorna una lista con todos los buses services dada una fecha y una ruta
	 * @param nFecha
	 * @param idRuta
	 * @return
	 */
	private LinkedList<Bus_ServiceVO> darBusesServicesFecha(Integer nFecha)
	{
		LinkedList<Bus_ServiceVO> retornar = new LinkedList<Bus_ServiceVO>();
		LinkedList<TripsVO> tripsL = darTripsServicios(nFecha);
		Node<TripsVO> currentTrip = tripsL.getHead();
		Node<Bus_ServiceVO> currentService = busesService.getHead();

		while(currentService!=null)
		{
			Bus_ServiceVO objServ = currentService.getData();
			while(currentTrip!=null)
			{
				TripsVO objTrip = currentTrip.getData();
				if(objServ.getTripId()==objTrip.getTrip_id() && Integer.parseInt(objServ.getRouteNo())== objTrip.getRoute_id())
				{
					retornar.add(objServ);
				}
				currentTrip = currentTrip.getNext();
			}
			currentService = currentService.getNext();
		}
		return retornar;
	}

	private TripsVO darTripID(Integer id)
	{
		TripsVO retornar = null;
		Node<TripsVO> trip = trips.getHead();
		MyInteger<TripsVO>[] ordTrips = ordenarTrips();
		int n = ordTrips.length;
		Integer cen = 0 , low = 0, high = n-1;
		while(low<=high)
		{ 
			cen = (low+high)/2;

			TripsVO objTrips = ordTrips[cen].getData();
			if (id== objTrips.getTrip_id())
			{
				retornar= objTrips;
			}

			else if(id < ordTrips[cen].getData().getRoute_id() ){
				high=cen-1;
			}
			else {
				low=cen+1;
			}
		}
		return retornar;
	}
	/**
	 * Retorna una lista con todas las paradas que tienen retardo en una fecha dada
	 * @param nFecha
	 * @param idRuta
	 * @return
	 */
	private LinkedList<MyInteger> darRetardosFecha(Integer nFecha)
	{
		LinkedList<MyInteger> retornar = null;
		LinkedList<Bus_ServiceVO> busesServices = darBusesServicesFecha(nFecha);
		Integer retardos = 0;
		Node<Bus_ServiceVO> currentService = busesService.getHead();

		LinkedList<Stop_timesVO> stops = darStopTimesFecha(nFecha); 
		Node<Stop_timesVO> currentStop = stops.getHead();
		while(currentStop!=null)
		{
			Stop_timesVO objStop = currentStop.getData();
			while(currentService!=null)
			{
				Bus_ServiceVO objServ = currentService.getData();
				if(objStop.getTrip_id()==objServ.getTripId())
				{
					if(objServ.getRecordedTime().compareTo(objStop.getArrival_time())>0)
					{
						Node<MyInteger> intCurr = retornar.getHead();
						while(intCurr!=null)
						{
							MyInteger inte = intCurr.getData();
							if(inte.getData()==objStop)
							{
								Integer ant = inte.getValue();
								inte.setValue(ant++);
							}
							else
							{
								MyInteger agr = new MyInteger(1);
								agr.setData(darTripID(objStop.getTrip_id()));
								retornar.add(agr);
							}
							intCurr = intCurr.getNext();
						}
					}
				}
				currentService = currentService.getNext();
			}
			currentStop = currentStop.getNext();
		}
		return retornar;
	}
	/**
	 * Retorna una lista con las paradas en las que hubo retardo en una fecha dada, ordenadas por n�mero de retardos
	 * @param nFecha Fecha de la que se quieren saber los retardos.
	 * @return Lista
	 * @throws Exception 
	 */
	public LinkedList<StopsVO> ITSparadasRetrasadasFecha(Integer nFecha) throws Exception
	{
		LinkedList<TripsVO> retornar = new LinkedList<TripsVO>();
		Integer fecha = nFecha;
		if(fecha != 20170821 && fecha != 20170822){
			throw new Exception("No hay datos para le fecha ingresada");
		}

		//Trips en una fecha y una ruta dada
		LinkedList<TripsVO> listaTrips = tripsEnFechaSolo(nFecha);	
		//Ordena los stop times
		MyInteger<Stop_timesVO>[] arrStopTimes = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);

		Node<TripsVO> tAct = listaTrips.getHead();
		for(int i = 0; i<arrStopTimes.length; i++){
			arrStopTimes[i].setValue(arrStopTimes[i].getData().getTrip_id());
		}
		//Saca todos los stop times de los trips
		arrStopTimes = radix.sort(arrStopTimes);

		LinkedList<Integer> stopsIds = new LinkedList<Integer>();

		while(tAct != null){
			int n = arrStopTimes.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrStopTimes[centro].getData().getTrip_id().intValue()==tAct.getData().getTrip_id().intValue()){
					stopsIds.add(arrStopTimes[centro].getData().getStop_id());
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<arrStopTimes.length && arrStopTimes[mayor].getData() != null  && !s1){
						if(arrStopTimes[mayor].getData().getTrip_id() == tAct.getData().getTrip_id()){
							stopsIds.add(arrStopTimes[mayor].getData().getStop_id());
							mayor += 1;
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && arrStopTimes[menor].getData() != null && !s2){
						if(arrStopTimes[menor].getData().getTrip_id() == tAct.getData().getTrip_id()){
							stopsIds.add(arrStopTimes[menor].getData().getStop_id());
							menor -=1;
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(tAct.getData().getTrip_id() < arrStopTimes[centro].getData().getTrip_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			tAct = tAct.getNext();
		}

		LinkedList<Integer> stopsIdsBien = new LinkedList<Integer>();
		Node<Integer> sAct = stopsIds.getHead();
		MyInteger<Integer>[] arrOrdStopsIds = (MyInteger<Integer>[]) listaArreglo(stopsIds);
		arrOrdStopsIds = radix.sort(arrOrdStopsIds);
		MyInteger<Integer> myIntAct = arrOrdStopsIds[0];
		stopsIdsBien.addAtEnd(myIntAct.getData());
		for(int i=0; i<arrOrdStopsIds.length; i++){
			MyInteger<Integer> actual = arrOrdStopsIds[i];
			if(myIntAct.getData().intValue() != actual.getData().intValue()){
				stopsIdsBien.addAtEnd(actual.getData());
				myIntAct = actual;
			}
		}


		MyInteger<StopsVO>[] arrOrdStops = (MyInteger<StopsVO>[]) listaArreglo(stops);
		for(int i = 0; i<arrOrdStops.length; i++){
			arrOrdStops[i].setValue(arrOrdStops[i].getData().getStop_id());
		}
		arrOrdStops = radix.sort(arrOrdStops);

		LinkedList<Integer> stopsCodes = new LinkedList<Integer>();
		Node<Integer> stopIdAct = stopsIdsBien.getHead();
		while(stopIdAct != null){
			int n = arrOrdStops.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStops[centro].getData().getStop_id()==stopIdAct.getData().intValue()){
					stopsCodes.add(arrOrdStops[centro].getData().getStop_code());
					break;
				}
				else if(stopIdAct.getData().intValue() < arrOrdStops[centro].getData().getStop_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			stopIdAct = stopIdAct.getNext();
		}

		/**
		 * Busca los stop estim service que tengan el mismo stop code que las paradas
		 */
		LinkedList<Stops_Estim_ServiceVO> stopEstServ = new LinkedList<Stops_Estim_ServiceVO>();
		MyInteger<Stops_Estim_ServiceVO>[] arrOrdStopsEstServ = (MyInteger<Stops_Estim_ServiceVO>[]) listaArreglo(stopsEServ);
		for(int i=0; i<arrOrdStopsEstServ.length; i++){
			arrOrdStopsEstServ[i].setValue(arrOrdStopsEstServ[i].getData().getStop_code());
		}
		arrOrdStopsEstServ = radix.sort(arrOrdStopsEstServ);

		Node<Integer> stopCodeAct = stopsCodes.getHead();
		while(stopCodeAct != null){
			int n = arrOrdStopsEstServ.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStopsEstServ[centro].getData().getStop_code().intValue()==stopCodeAct.getData().intValue()){
					stopEstServ.add(arrOrdStopsEstServ[centro].getData());
					break;
				}
				else if(stopCodeAct.getData().intValue() < arrOrdStopsEstServ[centro].getData().getStop_code().intValue() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			stopCodeAct = stopCodeAct.getNext();
		}


		LinkedList<Integer> stopEstServStopCodeRet = new LinkedList<Integer>();
		Node<Stops_Estim_ServiceVO> stESActual = stopEstServ.getHead();
		while(stESActual != null){
			Iterator<ScheduleVO> it = stESActual.getData().getSchedules().iterator();
			while(it.hasNext()){
				ScheduleVO scAct = it.next();
				if(scAct.getScheduleStatus().equals("-")){

					stopEstServStopCodeRet.add(stESActual.getData().getStop_code());

					break;
				}
			}
			stESActual = stESActual.getNext();
		}


		//Retorna los stop ids que se van a utilizar para retornar los trips al final
		LinkedList<MyInteger<StopsVO>> stopsFinal = new LinkedList<MyInteger<StopsVO>>();
		Node<Integer> intStopCodeAct = stopEstServStopCodeRet.getHead();
		Integer numeroVeces= 0;
		while(intStopCodeAct != null){
			int n = arrOrdStops.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStops[centro].getData().getStop_code()==intStopCodeAct.getData().intValue()){
					MyInteger<StopsVO> agregar = new MyInteger<StopsVO>(0);
					agregar.setData(arrOrdStops[centro].getData());
					agregar.setValue(arrOrdStops[centro].getData().getStop_id());
					stopsFinal.add(agregar);
					break;
				}
				else if(intStopCodeAct.getData().intValue() < arrOrdStops[centro].getData().getStop_code() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			intStopCodeAct = intStopCodeAct.getNext();
		}

		MyInteger<StopsVO>[] rt =(MyInteger<StopsVO>[]) listaMyIntegerArreglo(stopsFinal);
		rt = radix.sort(rt);
		System.out.println(rt.length);

		LinkedList<StopsVO> routesOrd = new LinkedList<StopsVO>();
		MyInteger<StopsVO>[] arrNueva = rt;
		Integer contar = 0;
		MyInteger<StopsVO> StopAct = arrNueva[0]; 
		routesOrd.add(arrNueva[0].getData());
		for(int i=0; i<arrNueva.length; i++){
			MyInteger<StopsVO> actual = arrNueva[i];
			if(StopAct.getData() != actual.getData()){
				routesOrd.addAtEnd(actual.getData());
				StopAct = actual;
			}
			else
			{
				actual.getData().aumentarVeces();
			}
		}
		MyInteger<StopsVO>[] fin =(MyInteger<StopsVO>[]) listaArreglo(routesOrd);
		for(int i = 0; i < fin.length;i++)
		{
			StopsVO ter = fin[i].getData();
			fin[i].setValue(ter.darVecesRetardado());
		}
		LinkedList<StopsVO> laUltima = new LinkedList<StopsVO>();
		rt = radix.sort(fin);
		for(int i = 0; i<rt.length;i++)
		{
			if(rt[i].getData().darVecesRetardado()!=0){
				laUltima.addAtEnd(rt[i].getData());
			}
		}
		/**Node<StopsVO> actUl = laUltima.getHead();

			/**while(actUl!=null)
			{
				StopsVO stop = actUl.getData();
				if(stop.darVecesRetardado()==0)
				{
					laUltima.delete(actUl);
				}
				actUl = actUl.getNext();
			}*/

		return laUltima;
	}

	//-----------------------------------------------------------------------------------------------------------
	// PUNTO 4A
	//-----------------------------------------------------------------------------------------------------------


	private LinkedList<TransfersVO> darTransfersRutaFecha(Integer idRuta, Integer nFecha)
	{
		LinkedList<TransfersVO> retornar = null;

		LinkedList<Stop_timesVO> stops = darStopTimesFechaRuta(nFecha, idRuta);
		Node<Stop_timesVO> currentStop = stops.getHead();


		Node<TransfersVO> transf = transfers.getHead();
		while(transf!=null)
		{
			TransfersVO trans = transf.getData();
			while(currentStop!=null)
			{
				Stop_timesVO objStop = currentStop.getData();
				if(trans.getFrom_stop_id()==objStop.getStop_id())
				{
					retornar.add(trans);
				}
				else if(trans.getTo_stop_id()==objStop.getStop_id())
				{
					retornar.add(trans);
				}
				currentStop = currentStop.getNext();
			}
			transf = transf.getNext();
		}
		return retornar;
	}
	/**
	 * Dice cuanto se demora en minutos una sucesión de transbordos
	 * @param idRuta
	 * @param nFecha
	 * @return
	 */
	private Integer darDuracionTransfersRutaFecha(Integer idRuta, Integer nFecha)
	{
		LinkedList<TransfersVO> transfers = darTransfersRutaFecha(idRuta, nFecha);
		Node<TransfersVO> transferAct = transfers.getHead();
		Integer retornar = 0;
		while(transferAct!=null)
		{
			TransfersVO objTr = transferAct.getData();
			retornar+= objTr.getMin_transfer_time();
			transferAct = transferAct.getNext();
		}
		return retornar;
	}
	private Stack<MyInteger> darParadasTransf(Integer nFecha, Integer idRuta) throws Exception
	{
		MyInteger<Stop_timesVO>[] ordStops = ordenarStopTimes();

		System.out.println(ordStops.length);

		LinkedList<TripsVO> lista = darTripsRutaServicio(nFecha, idRuta);
		Node<TripsVO> currentTrip = lista.getHead();


		Stack<MyInteger> stops = new Stack<MyInteger>();
		Node<MyInteger> nodoStop = null;
		MyInteger obj = new MyInteger<Object>(0);

		while(currentTrip!=null)
		{

			TripsVO objTrip = currentTrip.getData();
			int n = ordStops.length;
			Integer cen = 0 , low = 0, high = n-1;
			while(low<=high)
			{ cen = (low+high)/2;

			Stop_timesVO objStop = ordStops[cen].getData();
			if (objTrip.getTrip_id()== objStop.getTrip_id())
			{
				Integer duracion = 0;
				if(darTransfersRutaFecha(idRuta, nFecha)!=null)
				{
					duracion = darDuracionTransfersRutaFecha(idRuta, nFecha);
				}
				obj.setData(objStop);
				obj.setValue(duracion);
				stops.push(nodoStop);
			}

			else if(objTrip.getTrip_id() < ordStops[cen].getData().getTrip_id() ){
				high=cen-1;
			}
			else {
				low=cen+1;
			}
			}
			currentTrip = currentTrip.getNext();
		}
		return stops;

	}

	public LinkedList<Stop_timesVO> ITStransbordosRuta(Integer fecha,  Integer idRuta) throws Exception
	{
		LinkedList<Stop_timesVO> retornar = new LinkedList<Stop_timesVO>();
		Stack<MyInteger> stops = darParadasTransf(fecha, idRuta);
		MyInteger<Stop_timesVO>[]ordenar = agregarPilaMyIntArreglo(stops);
		MyInteger<Stop_timesVO>[]ordenada = radix.sort(ordenar);
		for(int i = 0; i< ordenada.length;i++)
		{
			Stop_timesVO stop = ordenada[i].getData();
			retornar.add(stop);
		}
		return retornar;

	}
	//-----------------------------------------------------------------------------------------------------------
	// PUNTO 5A
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Realiza búsqueda binaria entre los Stop_times y los trips, para encontrar los trips que pasan por determinada parada
	 * @param nFecha Fecha de la que se quieren saber los trips
	 * @return Lista con los trips
	 */
	private LinkedList<TripsVO> darTripsStops(Integer nFecha)
	{	
		MyInteger<Stop_timesVO>[] ordStops = ordenarStopTimes();

		LinkedList<TripsVO> lista = darTripsServicios(nFecha);
		Node<TripsVO> currentTrip = lista.getHead();


		LinkedList<TripsVO> retornar = new LinkedList<TripsVO>();

		while(currentTrip!=null)
		{

			TripsVO objTrip = currentTrip.getData();
			int n = ordStops.length;
			Integer cen = 0 , low = 0, high = n-1;
			while(low<=high)
			{ cen = (low+high)/2;

			Stop_timesVO objStop = ordStops[cen].getData();
			if (objTrip.getTrip_id()== objStop.getTrip_id())
			{
				retornar.add(objTrip);
			}

			else if(objTrip.getTrip_id() < ordStops[cen].getData().getTrip_id() ){
				high=cen-1;
			}
			else {
				low=cen+1;
			}

			currentTrip = currentTrip.getNext();
			}
		}
		return retornar;
	}
	/**
	 * Busca un trip según su id y la fecha dada por parámetro
	 * @param id
	 * @param nFecha
	 * @return
	 */
	private TripsVO buscarTripIDFecha(Integer id, Integer nFecha)
	{
		TripsVO retornar = null;
		MyInteger<TripsVO>[] ordTrips = ordenarTripsFechaID(nFecha);
		int n = ordTrips.length;
		Integer cen = 0 , low = 0, high = n-1;
		while(low<=high)
		{ 
			cen = (low+high)/2;

			TripsVO  objTrip = ordTrips[cen].getData();
			if (id== objTrip.getTrip_id())
			{
				retornar= objTrip;
			}

			else if(id < ordTrips[cen].getData().getRoute_id() ){
				high=cen-1;
			}
			else {
				low=cen+1;
			}
		}
		return retornar;
	}
	/**
	 * Retorna una lista con los trips que pasan por la stop dada por parámetro en una fecha dada por parámetro y una hora.
	 * @param nFecha
	 * @param horaIn
	 * @param horaFin
	 * @return
	 */
	private LinkedList<TripsVO> darTripsStopsFechaHora(Integer nFecha,Integer horaIn, Integer horaFin)
	{
		LinkedList<Bus_ServiceVO> buseServ = darBusesServicesFecha(nFecha);
		LinkedList<TripsVO> retornar = new LinkedList<TripsVO>();
		Node<Bus_ServiceVO> currServ = buseServ.getHead();
		while(currServ!=null)
		{
			Bus_ServiceVO objBus = currServ.getData();
			Integer horaBus = Integer.valueOf(objBus.getRecordedTime());
			if(horaBus<=horaIn && horaBus<=horaFin)
			{
				retornar.add(buscarTripIDFecha(objBus.getTripId(), nFecha));
			}
			else if(horaBus>horaIn&& horaBus<horaFin)
			{
				retornar.add(buscarTripIDFecha(objBus.getTripId(), nFecha));
			}
			currServ = currServ.getNext();
		}
		return retornar;
	}
	/**
	 * Retorna los trips que pasan por los stops dados por parámetros, en una fecha y rango de hora dado por parámetro
	 * @param horaFin
	 * @param horaIn
	 * @param nFecha
	 * @param stops
	 * @return
	 * @throws Exception
	 */
	private LinkedList<TripsVO> darTripsStopsFecha(Integer horaFin, Integer horaIn, Integer nFecha,Queue<Stop_timesVO> pStops) throws Exception
	{
		LinkedList<TripsVO> retornar = null;
		LinkedList<TripsVO> tri = darTripsStopsFechaHora(nFecha, horaIn, horaFin);
		Node<TripsVO> act = tri.getHead();
		Node<Stop_timesVO> nodeStop =  pStops.dequeue();
		while(act.getNext()!=null)
		{
			TripsVO obj = act.getData();
			while(nodeStop!=null)
			{
				Stop_timesVO stop = nodeStop.getData();
				if(stop.getTrip_id()==obj.getTrip_id())
				{
					retornar.add(obj);
				}
				nodeStop = pStops.dequeue();
			}

			act = act.getNext();
		}
		return retornar;
	}


	/**
	 * Retorna los stops de un stop time
	 * @param pStops
	 * @return
	 * @throws Exception
	 */
	private LinkedList<StopsVO> darStopsStopTimes(Queue<Stop_timesVO> pStops) throws Exception
	{
		Node<StopsVO> currStop = stops.getHead();
		Node<Stop_timesVO> nodeStop = pStops.dequeue();
		LinkedList<StopsVO> retornar = null;
		while(currStop.getNext()!=null)
		{
			StopsVO stop = currStop.getData();
			while(nodeStop!=null)
			{
				Stop_timesVO stopTime = nodeStop.getData();
				if(stop.getStop_id()==stopTime.getStop_id())
				{
					retornar.add(stop);
				}
				nodeStop= pStops.dequeue();
			}
			currStop = currStop.getNext();
		}
		return retornar;
	}
	/**
	 * Acá voy a comparar los stops con la parada, para sumar los tiempos
	 * @param horaFin 
	 * @param horaFin 
	 * @param horaIn 
	 * @param horaIn 
	 * @param nFecha 
	 * @param nFecha 
	 * @param pStops
	 * @param nFecha
	 * @param pStops 
	 * @param pStops 
	 * @return
	 * @throws Exception
	 */
	//private MyInteger<TripsVO>[] horaParadaTripStop(Queue<Stop_timesVO> pStops, Integer nFecha) throws Exception
	//{
	//LinkedList<Bus_ServiceVO> buses = darBusesServicesFecha(nFecha);
	//Node<Bus_ServiceVO> currBus = buses.getHead();
	//Node<Stop_timesVO> nodeStop = pStops.dequeue();
	//}
	/**
	 * Retorna una lista con todas las rutas que pasan por las paradas dadas por parámetro, en una fecha dada a una hora dada
	 * @param horaFin Hora fin a la que pasa
	 * @param horaIn Hora inicio a la que pasa
	 * @param nFecha Fecha en la que pasa
	 * @param pStops Stops por las que pasa
	 * @return Retorna una lista con las rutas que pasan por la zona dada.
	 * @throws Exception Excepción si la lista esta vacia
	 */
	private LinkedList<RoutesVO> darRutasStopsFechaHora(Integer horaFin, Integer horaIn, Integer nFecha, Queue<Stop_timesVO> pStops) throws Exception
	{
		LinkedList<TripsVO> trips = darTripsStopsFecha(horaFin, horaIn, nFecha, pStops);
		Node<TripsVO> tripAct = trips.getHead();
		LinkedList<RoutesVO> rutas = new LinkedList<RoutesVO>();
		while(tripAct!=null)
		{
			TripsVO objTrip = tripAct.getData();
			Integer rutaID = objTrip.getRoute_id();
			rutas.add(darRutaID(rutaID));
			tripAct = tripAct.getNext();
		}
		return rutas;
	}
	/**
	 * Retorna una lista de las rutas, ordenadas por ID, que pasan por unas paradas dadas en un rango de hora y una fecha dada
	 * @param horaFin Hora fin del rango de tiempo
	 * @param horaIn Hora inicio del rango de tiempo
	 * @param nFecha Fecha por la que pasan
	 * @param pStops Paradas por las que pasan
	 * @return Lista con todas las rutas que pasan por las paradas
	 * @throws Exception Excepcion si la lista está vacía
	 */
	public LinkedList<RoutesVO> darRutasxStopsFechaHora(Integer horaFin, Integer horaIn, Integer nFecha, Queue<Stop_timesVO> pStops) throws Exception
	{
		LinkedList<RoutesVO> rutas = darRutasStopsFechaHora(horaFin, horaIn, nFecha, pStops);
		MyInteger<RoutesVO>[] ordenar =(MyInteger<RoutesVO>[]) agregarListaArreglo(rutas);
		LinkedList<RoutesVO> retornar = new LinkedList<RoutesVO>();
		for(int i = 0; i<ordenar.length;i++)
		{
			RoutesVO ruta = ordenar[i].getData();
			ordenar[i].setValue(ruta.getRoute_id());
		}
		MyInteger<RoutesVO>[] ordenada = radix.sort(ordenar);
		for(int i =0; i<ordenada.length;i++)
		{
			RoutesVO ruta = ordenada[i].getData();
			retornar.add(ruta);
		}
		return retornar;
	}
	/**
	 * 
	 * @param ID
	 * @return
	 */
	private Stop_timesVO darStopTimesID(Integer ID)
	{
		Stop_timesVO retornar = null;
		Node<Stop_timesVO> routeStop = stopTimes.getHead();
		MyInteger<Stop_timesVO>[] ordStoptimes = ordenarStopTimes();
		int n = ordStoptimes.length;
		Integer cen = 0 , low = 0, high = n-1;
		while(low<=high)
		{ 
			cen = (low+high)/2;

			Stop_timesVO objStop = ordStoptimes[cen].getData();
			if (objStop.getStop_id()==ID)
			{
				retornar= objStop;
			}

			else if(ID < ordStoptimes[cen].getData().getStop_id() ){
				high=cen-1;
			}
			else {
				low=cen+1;
			}
		}
		return retornar;
	}
	/**
	 * Retorna los trips que pasan por los stops dados por par�metros, en una fecha y rango de hora dado por par�metro
	 * @param horaFin
	 * @param horaIn
	 * @param nFecha
	 * @param stops
	 * @return
	 * @throws Exception
	 */
	public LinkedList<TripsVO> ITSrutasPlanUtilizacion(String horaFin, String horaIn, String nFecha,String[] pStops) throws Exception
	{
		LinkedList<TripsVO> retornar = null;
		LinkedList<TripsVO> tri = darTripsStopsFechaHora(Integer.parseInt(nFecha), Integer.parseInt(horaIn), Integer.parseInt(horaFin));
		Node<TripsVO> act = tri.getHead();
		Queue<Stop_timesVO> qStops= null;
		for(int i = 0; i < pStops.length;i++)
		{
			Node agregar = new Node<Object>();
			agregar.setData(darStopTimesID(Integer.parseInt(pStops[i])));
			qStops.enqueue(agregar);
		}
		Node<Stop_timesVO> nodeStop =  qStops.dequeue();
		while(act.getNext()!=null)
		{
			TripsVO obj = act.getData();
			while(nodeStop!=null)
			{
				Stop_timesVO stop = nodeStop.getData();
				if(stop.getTrip_id()==obj.getTrip_id())
				{
					retornar.add(obj);
				}
				nodeStop = qStops.dequeue();
			}

			act = act.getNext();
		}
		return retornar;
	}




	//-----------------------------------------------------------------------------------------------------------
		// PUNTO 3C
		//-----------------------------------------------------------------------------------------------------------

		public LinkedList<Integer>	ITSretardosTrip(Integer nFecha, Integer nId)
		{

			MyInteger<Stop_timesVO>[] arrStopTimes = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);

			for(int i = 0; i<arrStopTimes.length; i++)
			{
				arrStopTimes[i].setValue(arrStopTimes[i].getData().getTrip_id());
			}
			arrStopTimes = radix.sort(arrStopTimes);

			LinkedList<Integer> stopsIds = new LinkedList<Integer>();
			int n = arrStopTimes.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrStopTimes[centro].getData().getTrip_id().intValue()==nId.intValue()){
					stopsIds.add(arrStopTimes[centro].getData().getStop_id());
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<arrStopTimes.length && arrStopTimes[mayor].getData() != null  && !s1){
						if(arrStopTimes[mayor].getData().getTrip_id() == nId.intValue()){
							stopsIds.add(arrStopTimes[mayor].getData().getStop_id());
							mayor += 1;
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && arrStopTimes[menor].getData() != null && !s2){
						if(arrStopTimes[menor].getData().getTrip_id() == nId.intValue()){
							stopsIds.add(arrStopTimes[menor].getData().getStop_id());
							menor -=1;
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(nId.intValue() < arrStopTimes[centro].getData().getTrip_id().intValue() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}


			LinkedList<Integer> stopsIdsBien = new LinkedList<Integer>();
			Node<Integer> sAct = stopsIds.getHead();
			MyInteger<Integer>[] arrOrdStopsIds = (MyInteger<Integer>[]) listaArreglo(stopsIds);
			for(int i=0; i<arrOrdStopsIds.length; i++){
				arrOrdStopsIds[i].setValue(arrOrdStopsIds[i].getData().intValue());
			}
			arrOrdStopsIds = radix.sort(arrOrdStopsIds);
			MyInteger<Integer> myIntAct = arrOrdStopsIds[0];
			stopsIdsBien.addAtEnd(myIntAct.getData());
			for(int i=0; i<arrOrdStopsIds.length; i++){
				MyInteger<Integer> actual = arrOrdStopsIds[i];
				if(myIntAct.getData().intValue() != actual.getData().intValue()){
					stopsIdsBien.addAtEnd(actual.getData());
					myIntAct = actual;
				}
			}
			MyInteger<StopsVO>[] arrOrdStops = (MyInteger<StopsVO>[]) listaArreglo(stops);
			for(int i = 0; i<arrOrdStops.length; i++){
				arrOrdStops[i].setValue(arrOrdStops[i].getData().getStop_id());
			}
			arrOrdStops = radix.sort(arrOrdStops);
			

			LinkedList<Integer> stopsCodes = new LinkedList<Integer>();
			Node<Integer> stopIdAct = stopsIdsBien.getHead();
			while(stopIdAct != null){
				int m = arrOrdStops.length;
				int center,less=0,max=m-1;
				while(less<=max){
					center=(max+less)/2;
					if(arrOrdStops[center].getData().getStop_id()==stopIdAct.getData().intValue()){
						stopsCodes.add(arrOrdStops[center].getData().getStop_code());
						break;
					}
					else if(stopIdAct.getData().intValue() < arrOrdStops[center].getData().getStop_id() ){
						max=center-1;
					}
					else {
						less=center+1;
					}
				}
				stopIdAct = stopIdAct.getNext();
			}
		

			LinkedList<Stops_Estim_ServiceVO> stopEstServ = new LinkedList<Stops_Estim_ServiceVO>();
			MyInteger<Stops_Estim_ServiceVO>[] arrOrdStopsEstServ = (MyInteger<Stops_Estim_ServiceVO>[]) listaArreglo(stopsEServ);
			for(int i=0; i<arrOrdStopsEstServ.length; i++){
				arrOrdStopsEstServ[i].setValue(arrOrdStopsEstServ[i].getData().getStop_code());
			}
			arrOrdStopsEstServ = radix.sort(arrOrdStopsEstServ);

			Node<Integer> stopCodeAct = stopsCodes.getHead();
			while(stopCodeAct != null){
				int z = arrOrdStopsEstServ.length;
				int cen,in=0,su=z-1;
				while(in<=su){
					cen=(su+in)/2;
					if(arrOrdStopsEstServ[cen].getData().getStop_code().intValue()==stopCodeAct.getData().intValue()){
						stopEstServ.add(arrOrdStopsEstServ[cen].getData());
						break;
					}
					else if(stopCodeAct.getData().intValue() < arrOrdStopsEstServ[cen].getData().getStop_code().intValue() ){
						su=cen-1;
					}
					else {
						in=cen+1;
					}
				}
				stopCodeAct = stopCodeAct.getNext();
			}
			


			LinkedList<Integer> stopEstServStopCodeRet = new LinkedList<Integer>();
			Node<Stops_Estim_ServiceVO> stESActual = stopEstServ.getHead();
			while(stESActual != null){
				Iterator<ScheduleVO> it = stESActual.getData().getSchedules().iterator();
				while(it.hasNext()){
					ScheduleVO scAct = it.next();
					if(scAct.getScheduleStatus().equals("-")){
						stopEstServStopCodeRet.add(stESActual.getData().getStop_code());
					}
				}
				stESActual = stESActual.getNext();
			}
			
			Node<Integer> prueba = stopEstServStopCodeRet.getHead();
			LinkedList<Integer> retornarFinal= new LinkedList<Integer>();
			MyInteger<Integer>[] arrNueva = (MyInteger<Integer>[]) listaArreglo(stopEstServStopCodeRet);
			MyInteger<Integer> NumAct = arrNueva[0]; 
			retornarFinal.add(arrNueva[0].getData());
			for(int i=0; i<arrNueva.length; i++){
				MyInteger<Integer> actual = arrNueva[i];
				if(NumAct.getData() != actual.getData()){
					retornarFinal.addAtEnd(actual.getData());
					NumAct = actual;
				}
			}
			return retornarFinal;
		}
	//-------------------------------------------------------------------------------------
	//PUNTO 4C
	//-------------------------------------------------------------------------------------
	@Override
	public LinkedList<Integer> ITSparadasCompartidas(String nFecha)
	{
		LinkedList<TripsVO> lTrips = tripsEnFechaSolo(Integer.parseInt(nFecha));
		LinkedList<MyInteger<TripsVO>> myList = new LinkedList<>();

		Node<TripsVO> tAct = lTrips.getHead();
		while(tAct != null){
			MyInteger<TripsVO> nuevo = new MyInteger(tAct.getData().getRoute_id().intValue());
			nuevo.setData(tAct.getData());
			myList.add(nuevo);
			tAct = tAct.getNext();
		}

		MyInteger<Stop_timesVO>[] arrStopTimes = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);
		for(int i =0; i<arrStopTimes.length; i++){
			arrStopTimes[i].setValue(arrStopTimes[i].getData().getTrip_id());
		}
		arrStopTimes = radix.sort(arrStopTimes);

		LinkedList<MyInteger<Integer>> listaStops = new LinkedList<>();
		Node<MyInteger<TripsVO>> myAct = myList.getHead();
		while(myAct != null){
			int n = arrStopTimes.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrStopTimes[centro].getData().getTrip_id().intValue()==myAct.getData().getData().getTrip_id().intValue()){
					MyInteger<Integer> nuevo = new MyInteger(arrStopTimes[centro].getData().getStop_id().intValue());
					nuevo.setData(myAct.getData().getValue());
					listaStops.add(nuevo);
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<arrStopTimes.length && arrStopTimes[mayor].getData() != null  && !s1){
						if(arrStopTimes[mayor].getData().getTrip_id() == myAct.getData().getData().getTrip_id()){
							MyInteger<Integer> nuevo2 = new MyInteger(arrStopTimes[mayor].getData().getStop_id().intValue());
							nuevo2.setData(myAct.getData().getValue());
							listaStops.add(nuevo2);
							mayor += 1;
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && arrStopTimes[menor].getData() != null && !s2){
						if(arrStopTimes[menor].getData().getTrip_id() == myAct.getData().getData().getTrip_id()){
							MyInteger<Integer> nuevo3 = new MyInteger(arrStopTimes[menor].getData().getStop_id().intValue());
							nuevo3.setData(myAct.getData().getValue());
							listaStops.add(nuevo3);
							menor -=1;
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(myAct.getData().getData().getTrip_id().intValue() < arrStopTimes[centro].getData().getTrip_id().intValue() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			myAct = myAct.getNext();
		}

		LinkedList<MyInteger<Integer>> listaMyInt = new LinkedList<>(); 
		Node<MyInteger<Integer>> sAct = listaStops.getHead();
		while(sAct != null){
			Node<MyInteger<Integer>> sig = sAct.getNext();
			MyInteger<Integer> nuevo = new MyInteger(0);
			while(sig != null && sig.getData().getData().intValue() == sAct.getData().getData().intValue()){
				if(sig.getData().intValue() == sAct.getData().intValue()){
					nuevo.setData(sig.getData().intValue());
					nuevo.sumValue(1);
					listaMyInt.addAtEnd(nuevo);
				}
				sig = sig.getNext();
			}
			sAct =sAct.getNext();
		}

		LinkedList<Integer> def = new LinkedList<>();
		Node<MyInteger<Integer>> defAct = listaMyInt.getHead();
		while(defAct != null){
			def.add(defAct.getData().getData());
			defAct = defAct.getNext();
		}


		return def;
	}

	//--------------------------------------------------------------------------------------
	//-----------------------------------------PARTE MAURICIO-------------------------------
	//--------------------------------------------------------------------------------------

	@Override
	public LinkedList<RoutesVO> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha)throws Exception {

		//Me ahorro mucha complejidad con esto:
		String[] partes = nombreEmpresa.split(" ");
		if(nombreEmpresa.equalsIgnoreCase("TransLink")) {
			nombreEmpresa = "CMBC";
		}
		else if(nombreEmpresa.equalsIgnoreCase("West Coast Express")) {
			nombreEmpresa = "WCEX";
		}
		else if(nombreEmpresa.equalsIgnoreCase("British Columbia Rapid Transit Company")) {
			nombreEmpresa = "SKYT";
		}
		else {
			throw new Exception("No existe ninguna empresa con el nombre dado");}
		//

		LinkedList<CalendarVO> serviceIds = new LinkedList<CalendarVO>();
		Node<CalendarVO> cActual = calendar.getHead();
		while(cActual != null){
			CalendarVO  act = cActual.getData();
			int fechAct = Integer.parseInt(fecha);
			if((act.getStart_date()<=fechAct) && (fechAct<=act.getEnd_date())){
				serviceIds.add(act);
			}
			cActual = cActual.getNext();
		}

		if(serviceIds.getSize() == 0) {
			throw new Exception("No se encontraron rutas con la fecha dada");
		}

		LinkedList<Integer> rutasIds = new LinkedList<Integer>();
		Node<TripsVO> tActual = trips.getHead();
		while(tActual != null){
			TripsVO act = tActual.getData();
			Node<CalendarVO> iAct = serviceIds.getHead();
			while(iAct != null){
				if(act.getService_id() == iAct.getData().getService_id()){
					rutasIds.add(act.getRoute_id());
				}
				iAct = iAct.getNext();
			}
			tActual = tActual.getNext();
		}

		LinkedList<Integer> rutasIdsOrd = new LinkedList<>();
		MyInteger<Integer>[] arrNuevaL = (MyInteger<Integer>[]) listaArreglo(rutasIds);
		arrNuevaL = radix.sort(arrNuevaL);
		MyInteger<Integer> intAct = arrNuevaL[0]; 
		rutasIdsOrd.add(arrNuevaL[0].getData());
		for(int i=0; i<arrNuevaL.length; i++){
			MyInteger<Integer> actual = arrNuevaL[i];
			if(intAct.getData().intValue() != actual.getData().intValue()){
				rutasIdsOrd.add(actual.getData());
				intAct = actual;
			}
		}


		if(rutasIds.getSize() == 0) {
			throw new Exception("No se encontraron rutas con la fecha dada");
		}

		LinkedList<RoutesVO> rutas = new LinkedList<>();
		Node<Integer> iAct = rutasIdsOrd.getHead();
		while(iAct!=null){
			Node<RoutesVO> rAct = routes.getHead();
			while(rAct != null){
				if(iAct.getData().intValue() == rAct.getData().getRoute_id() && rAct.getData().getAgency_id().equals(nombreEmpresa)){
					rutas.add(rAct.getData());
				}
				rAct = rAct.getNext();
			}
			iAct = iAct.getNext();
		}

		if(rutas.getSize() == 0) {
			throw new Exception("No se encontraron rutas con la empresa dada");
		}

		/**		
		System.out.println(1);

		LinkedList<TripsVO> viajes = new LinkedList<>();
		MyInteger<TripsVO>[] arrViajes = (MyInteger<TripsVO>[]) listaArreglo(trips);
		Node<RoutesVO>rutaAct = rutas.getHead();
		while(rutaAct != null){
			RoutesVO act = rutaAct.getData();
			int n = arrViajes.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrViajes[centro].getData().getRoute_id() == act.getRoute_id()){
					viajes.add(arrViajes[centro].getData());
					//System.out.println(act.getRoute_id());
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<arrViajes.length && arrViajes[mayor].getData() != null  && !s1){
						if(arrViajes[mayor].getData().getRoute_id() == act.getRoute_id()){
							viajes.add(arrViajes[mayor].getData());
							mayor += 1;
							//System.out.println(act.getRoute_id());
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && arrViajes[menor].getData() != null && !s2){
						if(arrViajes[menor].getData().getRoute_id() == act.getRoute_id()){
							viajes.add(arrViajes[menor].getData());
							menor -=1;
							//System.out.println(act.getRoute_id());
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(act.getRoute_id() < arrViajes[centro].getData().getRoute_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			rutaAct = rutaAct.getNext();
		}
		System.out.println(2);


		LinkedList<MyInteger<TripsVO>> nPPviajes = new LinkedList<>();
		MyInteger<Stop_timesVO>[] arrStopT = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);
		Node<TripsVO> vAct = viajes.getHead();
		while(vAct != null){
			TripsVO act = vAct.getData();
			MyInteger<TripsVO> myAct = new MyInteger<>(0);
			myAct.setData(act);
			nPPviajes.add(myAct);
			//Node<Stop_timesVO> stAct = stopTimes.getHead();
			int n = arrStopT.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrStopT[centro].getData().getTrip_id() == act.getTrip_id()){

					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<arrStopT.length && arrStopT[mayor].getData() != null  && !s1){
						if(arrStopT[mayor].getData().getTrip_id() == act.getTrip_id()){
							myAct.sumValue(1);
							mayor += 1;
							//System.out.println(act.getTrip_id());
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && arrStopT[mayor].getData() != null  && !s2){
						if(arrStopT[menor].getData().getTrip_id() == act.getTrip_id()){
							myAct.sumValue(1);
							menor -=1;
							//System.out.println(act.getTrip_id());
						}
						else{
							s2=true;
						}
					}
					break;

				}
				else if(act.getTrip_id() < arrStopT[centro].getData().getTrip_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			vAct = vAct.getNext();
		}
		System.out.println(3);

		LinkedList<MyInteger<Integer>> list = new LinkedList<>();
		MyInteger<TripsVO>[] arrV = (MyInteger<TripsVO>[]) listaArreglo(viajes);
		Node<MyInteger<TripsVO>> mAct = nPPviajes.getHead();
		while(mAct != null){
			//Node<TripsVO> viajeAct = viajes.getHead();
			MyInteger<Integer> nuevo = new MyInteger<Integer>(mAct.getData().intValue());
			int n = arrV.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrV[centro].getData().getTrip_id()==mAct.getData().getData().getTrip_id()){
					list.add(nuevo);
					nuevo.setData(arrV[centro].getData().getRoute_id());
					//System.out.println(arrV[centro].getData().getRoute_id());
					break;
				}
				else if(mAct.getData().getData().getTrip_id() <  arrV[centro].getData().getTrip_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			mAct = mAct.getNext();
		}
		System.out.println(4);

		/**Node<MyInteger<Integer>> nodoAct = list.getHead();
		while(nodoAct != null){
			System.out.println(nodoAct.getData().getData().intValue());
			nodoAct = nodoAct.getNext();
		}

		MyInteger<Integer>[] listaInt = new MyInteger[248];
		Node<MyInteger<Integer>> intAct2 = list.getHead();
		int cont = 0;
		MyInteger<Integer> nuevo = new MyInteger(0);
		nuevo.setData(intAct2.getData().getData());
		while(intAct2 != null){
			Node<MyInteger<Integer>> sig = intAct2.getNext();
			while(sig != null){
				if(nuevo.getData() == sig.getData().getData()){
					nuevo.sumValue(sig.getData().getData());
					list.delete(sig);
					sig = intAct2.getNext();
				}
				else{
					sig = sig.getNext();
				}
			}
			//System.out.println(cont);
			listaInt[cont] = nuevo;
			cont++;
			intAct2 = intAct2.getNext();
		}
		//System.out.println(5);


		/**for(int i = 0; i<listaInt.length; i++){
			System.out.println(listaInt[i].intValue());
		}**/

		/** MyInteger<RoutesVO>[] listaRutas = new MyInteger[248];
        Node<MyInteger<Integer>> miAct = list.getHead();
        while(miAct != null){
        	Node<RoutesVO> rutaAct = routes.getHead();
        	MyInteger<RoutesVO> nuevo = new MyInteger(0);
        	while(rutaAct != null){
        		if()
        	}
        }
		 **/
		return rutas;
	}


	@Override
	public LinkedList<TripsVO> ITSviajesRetrasoTotalRuta(String pIdRuta, String pFecha)throws Exception {
		Integer fecha = Integer.parseInt(pFecha);
		if(fecha != 20170821 && fecha != 20170822){
			throw new Exception("No hay datos para le fecha ingresada");
		}
		Integer idRuta = Integer.parseInt(pIdRuta);

		LinkedList<TripsVO> listaTrips = tripsEnFecha(idRuta, fecha);	
		MyInteger<Stop_timesVO>[] arrStopTimes = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);
		Node<TripsVO> tAct = listaTrips.getHead();
		for(int i = 0; i<arrStopTimes.length; i++){
			arrStopTimes[i].setValue(arrStopTimes[i].getData().getTrip_id());
		}
		arrStopTimes = radix.sort(arrStopTimes);

		LinkedList<Integer> stopsIds = new LinkedList<>();
		while(tAct != null){
			int n = arrStopTimes.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrStopTimes[centro].getData().getTrip_id().intValue()==tAct.getData().getTrip_id().intValue()){
					stopsIds.add(arrStopTimes[centro].getData().getStop_id());
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<arrStopTimes.length && arrStopTimes[mayor].getData() != null  && !s1){
						if(arrStopTimes[mayor].getData().getTrip_id() == tAct.getData().getTrip_id()){
							stopsIds.add(arrStopTimes[mayor].getData().getStop_id());
							mayor += 1;
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && arrStopTimes[menor].getData() != null && !s2){
						if(arrStopTimes[menor].getData().getTrip_id() == tAct.getData().getTrip_id()){
							stopsIds.add(arrStopTimes[menor].getData().getStop_id());
							menor -=1;
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(tAct.getData().getTrip_id() < arrStopTimes[centro].getData().getTrip_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			tAct = tAct.getNext();
		}

		LinkedList<Integer> stopsIdsBien = new LinkedList<>();
		Node<Integer> sAct = stopsIds.getHead();
		MyInteger<Integer>[] arrOrdStopsIds = (MyInteger<Integer>[]) listaArreglo(stopsIds);
		for(int i=0; i<arrOrdStopsIds.length; i++){
			arrOrdStopsIds[i].setValue(arrOrdStopsIds[i].getData().intValue());
		}
		arrOrdStopsIds = radix.sort(arrOrdStopsIds);
		MyInteger<Integer> myIntAct = arrOrdStopsIds[0];
		stopsIdsBien.addAtEnd(myIntAct.getData());
		for(int i=0; i<arrOrdStopsIds.length; i++){
			MyInteger<Integer> actual = arrOrdStopsIds[i];
			if(myIntAct.getData().intValue() != actual.getData().intValue()){
				stopsIdsBien.addAtEnd(actual.getData());
				myIntAct = actual;
			}
		}
		MyInteger<StopsVO>[] arrOrdStops = (MyInteger<StopsVO>[]) listaArreglo(stops);
		for(int i = 0; i<arrOrdStops.length; i++){
			arrOrdStops[i].setValue(arrOrdStops[i].getData().getStop_id());
		}
		arrOrdStops = radix.sort(arrOrdStops);

		LinkedList<Integer> stopsCodes = new LinkedList<>();
		Node<Integer> stopIdAct = stopsIdsBien.getHead();
		while(stopIdAct != null){
			int n = arrOrdStops.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStops[centro].getData().getStop_id()==stopIdAct.getData().intValue()){
					stopsCodes.add(arrOrdStops[centro].getData().getStop_code());
					break;
				}
				else if(stopIdAct.getData().intValue() < arrOrdStops[centro].getData().getStop_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			stopIdAct = stopIdAct.getNext();
		}

		LinkedList<Stops_Estim_ServiceVO> stopEstServ = new LinkedList<>();
		MyInteger<Stops_Estim_ServiceVO>[] arrOrdStopsEstServ = (MyInteger<Stops_Estim_ServiceVO>[]) listaArreglo(stopsEServ);
		for(int i=0; i<arrOrdStopsEstServ.length; i++){
			arrOrdStopsEstServ[i].setValue(arrOrdStopsEstServ[i].getData().getStop_code());
		}
		arrOrdStopsEstServ = radix.sort(arrOrdStopsEstServ);

		Node<Integer> stopCodeAct = stopsCodes.getHead();
		while(stopCodeAct != null){
			int n = arrOrdStopsEstServ.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStopsEstServ[centro].getData().getStop_code().intValue()==stopCodeAct.getData().intValue()){
					stopEstServ.add(arrOrdStopsEstServ[centro].getData());
					break;
				}
				else if(stopCodeAct.getData().intValue() < arrOrdStopsEstServ[centro].getData().getStop_code().intValue() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			stopCodeAct = stopCodeAct.getNext();
		}

		LinkedList<Integer> stopEstServStopCodeRet = new LinkedList<>();
		Node<Stops_Estim_ServiceVO> stESActual = stopEstServ.getHead();
		while(stESActual != null){
			Iterator<ScheduleVO> it = stESActual.getData().getSchedules().iterator();
			while(it.hasNext()){
				ScheduleVO scAct = it.next();
				if(scAct.getScheduleStatus().equals("-")){
					stopEstServStopCodeRet.add(stESActual.getData().getStop_code());
				}
			}
			stESActual = stESActual.getNext();
		}

		LinkedList<Integer> stopsIdsDef = new LinkedList<>();
		Node<Integer> intStopCodeAct = stopEstServStopCodeRet.getHead();
		while(intStopCodeAct != null){
			int n = arrOrdStops.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStops[centro].getData().getStop_code()==intStopCodeAct.getData().intValue()){
					stopsIdsDef.add(arrOrdStops[centro].getData().getStop_id());
					break;
				}
				else if(intStopCodeAct.getData().intValue() < arrOrdStops[centro].getData().getStop_code() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			intStopCodeAct = intStopCodeAct.getNext();
		}

		LinkedList<Integer> listaTripsIdsDef = new LinkedList<>();
		Node<Integer> stopIdDefAct = stopsIdsDef.getHead();
		MyInteger<Stop_timesVO>[] arrOrdStopT = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);
		for(int i = 0; i<arrOrdStopT.length; i++){
			arrOrdStopT[i].setValue(arrOrdStopT[i].getData().getStop_id());
		}
		arrOrdStopT = radix.sort(arrOrdStopT);

		while(stopIdDefAct != null){
			int n = arrOrdStopT.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStopT[centro].getData().getStop_id()==stopIdDefAct.getData().intValue()){
					listaTripsIdsDef.add(arrOrdStopT[centro].getData().getTrip_id());
					break;
				}
				else if(stopIdDefAct.getData().intValue() < arrOrdStopT[centro].getData().getStop_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			stopIdDefAct = stopIdDefAct.getNext();
		}

		LinkedList<Integer> tripsIdsNoR = new LinkedList<>();
		MyInteger<Integer>[] arrlistaTripsIds = (MyInteger<Integer>[]) listaArreglo(listaTripsIdsDef);
		for(int i = 0; i<arrlistaTripsIds.length; i++){
			arrlistaTripsIds[i].setValue(arrlistaTripsIds[i].getData());
		}
		arrlistaTripsIds = radix.sort(arrlistaTripsIds);

		MyInteger<Integer> intAct = arrlistaTripsIds[0]; 
		tripsIdsNoR.add(arrlistaTripsIds[0].getData());
		for(int i=0; i<arrlistaTripsIds.length; i++){
			MyInteger<Integer> actual = arrlistaTripsIds[i];
			if(intAct.getData().intValue() != actual.getData().intValue()){
				tripsIdsNoR.add(actual.getData());
				intAct = actual;
			}
		}
		MyInteger<Stop_timesVO>[] arrOrdStopTrips = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);
		for(int i = 0; i<arrOrdStopTrips.length; i++){
			arrOrdStopTrips[i].setValue(arrOrdStopTrips[i].getData().getTrip_id());
		}
		arrOrdStopTrips = radix.sort(arrOrdStopTrips);
		System.out.println();

		/**LinkedList<Integer> idTripsDef = new LinkedList<>();
		Node<Integer> idAct = tripsIdsNoR.getHead();
		while(idAct != null){
			LinkedList<Stop_timesVO> lista = new LinkedList<>();
			int n = arrOrdStopTrips.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStopTrips[centro].getData().getTrip_id().intValue()==idAct.getData().intValue()){
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<arrOrdStopTrips.length && arrOrdStopTrips[mayor].getData() != null  && !s1){
						if(arrOrdStopTrips[mayor].getData().getTrip_id().intValue() == idAct.getData().intValue()){
							lista.add(arrOrdStopTrips[mayor].getData());
							mayor += 1;
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && arrOrdStopTrips[menor].getData() != null && !s2){
						if(arrOrdStopTrips[menor].getData().getTrip_id().intValue() == idAct.getData().intValue()){
							lista.add(arrOrdStopTrips[menor].getData());
							menor -=1;
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(idAct.getData().intValue() < arrOrdStopTrips[centro].getData().getTrip_id().intValue() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}

			MyInteger<Stop_timesVO>[] arrLista = (MyInteger<Stop_timesVO>[]) listaArreglo(lista);
			for(int i = 0; i<arrLista.length; i++){
				arrLista[i].setValue(arrLista[i].getData().getStop_sequence());
			}
			arrlistaTripsIds = radix.sort(arrlistaTripsIds);

			MyInteger<Integer>[] arrStopsIds = (MyInteger<Integer>[]) listaArreglo(stopsIdsDef);
			for(int i = 0; i<arrStopsIds.length; i++){
				arrStopsIds[i].setValue(arrStopsIds[i].getData().intValue());
			}
			arrStopsIds = radix.sort(arrStopsIds);

			for(int i =0; i<arrStopsIds.length; i++){
				System.out.println(arrStopsIds[i].getData().intValue());
			}

			for(int i =0; i<arrLista.length; i++){
				Stop_timesVO act = arrLista[i].getData();
				int z = arrStopsIds.length;
				int cent,inferior=0,superior=z-1;
				while(inferior<=superior){
					cent=(superior+inferior)/2;
					if(arrStopsIds[cent].getData().intValue()==act.getStop_id().intValue()){
						int j = i;
						Stop_timesVO temp = (j<arrLista.length)?arrLista[j].getData():null;
						boolean boo = false;
						while(!boo && temp != null && j<arrLista.length){
							boolean encontro = false;
							int w = arrStopsIds.length;
							int center,less=0,max=w-1;
							while(less<=max){
								center=(max+less)/2;
								if(arrStopsIds[center].getData().intValue()==temp.getStop_id().intValue()){
									encontro = true;
									break;
								}
								else if(temp.getStop_id().intValue() < arrStopsIds[center].getData().intValue() ){
									max=center-1;
								}
								else {
									less=center+1;
								}
							}
							if(encontro == false){
								boo = true;
							}
							j++;
						}
						if(boo == false){
							idTripsDef.add(idAct.getData());
						}
					}
					else if(act.getStop_id().intValue() < arrStopsIds[cent].getData().intValue() ){
						superior=cent-1;
					}
					else {
						inferior=cent+1;
					}
				}
			}

			idAct = idAct.getNext();
		}**/

		//System.out.println(idTripsDef.getSize());

		/**LinkedList<TripsVO> listaTripsDef = new LinkedList<>();
		Node<Integer> tripIdDefAct = listaTripsIdsDef.getHead();
		MyInteger<TripsVO>[] arrOrdTrips = (MyInteger<TripsVO>[]) listaArreglo(trips);
		for(int i=0; i<arrOrdTrips.length; i++){
			arrOrdTrips[i].setValue(arrOrdTrips[i].getData().getTrip_id());
		}
		arrOrdTrips = radix.sort(arrOrdTrips);

		while(tripIdDefAct != null){
			int n = arrOrdTrips.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdTrips[centro].getData().getTrip_id()==tripIdDefAct.getData().intValue()){
					listaTripsDef.add(arrOrdTrips[centro].getData());
					break;
				}
				else if(tripIdDefAct.getData().intValue() < arrOrdTrips[centro].getData().getTrip_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			tripIdDefAct = tripIdDefAct.getNext();
		}
		return listaTripsDef;**/
		return null;
	}

	@Override
	public LinkedList<String> ITSretardoHoraRuta(String pIdRuta, String pFecha)throws Exception {
		String H0A = "0:00-1:00";
		Integer h0=0;
		String H1A = "1:00-2:00";
		Integer h1=0;
		String H2A = "2:00-3:00";
		Integer h2=0;
		String H3A = "3:00-4:00";
		Integer h3=0;
		String H4A = "4:00-5:00";
		Integer h4=0;
		String H5A = "5:00-6:00";
		Integer h5=0;
		String H6A = "6:00-7:00";
		Integer h6=0;
		String H7A = "7:00-8:00";
		Integer h7=0;
		String H8A = "8:00-9:00";
		Integer h8=0;
		String H9A = "9:00-10:00";
		Integer h9=0;
		String H10A = "10:00-11:00";
		Integer h10=0;
		String H11A = "11:00-12:00";
		Integer h11=0;
		String H12A = "12:00-13:00";
		Integer h12=0;
		String H13A = "13:00-14:00";
		Integer h13=0;
		String H14A = "14:00-15:00";
		Integer h14=0;
		String H15A = "15:00-16:00";
		Integer h15=0;
		String H16A = "16:00-17:00";
		Integer h16=0;
		String H17A = "17:00-18:00";
		Integer h17=0;
		String H18A = "18:00-19:00";
		Integer h18=0;
		String H19A = "19:00-20:00";
		Integer h19=0;
		String H20A = "20:00-21:00";
		Integer h20=0;
		String H21A = "21:00-22:00";
		Integer h21=0;
		String H22A = "22:00-23:00";
		Integer h22=0;
		String H23A = "23:00-24:00";
		Integer h23=0;

		Integer fecha = Integer.parseInt(pFecha);
		if(fecha != 20170821 && fecha != 20170822){
			throw new Exception("No hay datos para le fecha ingresada");
		}
		Integer idRuta = Integer.parseInt(pIdRuta);

		LinkedList<TripsVO> listaTrips = tripsEnFecha(idRuta, fecha);	
		MyInteger<Stop_timesVO>[] arrStopTimes = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);
		Node<TripsVO> tAct = listaTrips.getHead();
		for(int i = 0; i<arrStopTimes.length; i++){
			arrStopTimes[i].setValue(arrStopTimes[i].getData().getTrip_id());
		}
		arrStopTimes = radix.sort(arrStopTimes);
		LinkedList<Integer> stopsIds = new LinkedList<>();
		while(tAct != null){
			int n = arrStopTimes.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrStopTimes[centro].getData().getTrip_id().intValue()==tAct.getData().getTrip_id().intValue()){
					stopsIds.add(arrStopTimes[centro].getData().getStop_id());
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<arrStopTimes.length && arrStopTimes[mayor].getData() != null  && !s1){
						if(arrStopTimes[mayor].getData().getTrip_id() == tAct.getData().getTrip_id()){
							stopsIds.add(arrStopTimes[mayor].getData().getStop_id());
							mayor += 1;
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && arrStopTimes[menor].getData() != null && !s2){
						if(arrStopTimes[menor].getData().getTrip_id() == tAct.getData().getTrip_id()){
							stopsIds.add(arrStopTimes[menor].getData().getStop_id());
							menor -=1;
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(tAct.getData().getTrip_id() < arrStopTimes[centro].getData().getTrip_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			tAct = tAct.getNext();
		}

		LinkedList<Integer> stopsIdsBien = new LinkedList<>();
		Node<Integer> sAct = stopsIds.getHead();
		MyInteger<Integer>[] arrOrdStopsIds = (MyInteger<Integer>[]) listaArreglo(stopsIds);
		arrOrdStopsIds = radix.sort(arrOrdStopsIds);
		MyInteger<Integer> myIntAct = arrOrdStopsIds[0];
		stopsIdsBien.addAtEnd(myIntAct.getData());
		for(int i=0; i<arrOrdStopsIds.length; i++){
			MyInteger<Integer> actual = arrOrdStopsIds[i];
			if(myIntAct.getData().intValue() != actual.getData().intValue()){
				stopsIdsBien.addAtEnd(actual.getData());
				myIntAct = actual;
			}
		}

		MyInteger<StopsVO>[] arrOrdStops = (MyInteger<StopsVO>[]) listaArreglo(stops);
		for(int i = 0; i<arrOrdStops.length; i++){
			arrOrdStops[i].setValue(arrOrdStops[i].getData().getStop_id());
		}
		arrOrdStops = radix.sort(arrOrdStops);
		LinkedList<Integer> stopsCodes = new LinkedList<>();
		Node<Integer> stopIdAct = stopsIdsBien.getHead();
		while(stopIdAct != null){
			int n = arrOrdStops.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStops[centro].getData().getStop_id()==stopIdAct.getData().intValue()){
					stopsCodes.add(arrOrdStops[centro].getData().getStop_code());
					break;
				}
				else if(stopIdAct.getData().intValue() < arrOrdStops[centro].getData().getStop_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			stopIdAct = stopIdAct.getNext();
		}

		LinkedList<Stops_Estim_ServiceVO> stopEstServ = new LinkedList<>();
		MyInteger<Stops_Estim_ServiceVO>[] arrOrdStopsEstServ = (MyInteger<Stops_Estim_ServiceVO>[]) listaArreglo(stopsEServ);
		for(int i=0; i<arrOrdStopsEstServ.length; i++){
			arrOrdStopsEstServ[i].setValue(arrOrdStopsEstServ[i].getData().getStop_code());
		}
		arrOrdStopsEstServ = radix.sort(arrOrdStopsEstServ);
		System.out.println(4);
		Node<Integer> stopCodeAct = stopsCodes.getHead();
		while(stopCodeAct != null){
			int n = arrOrdStopsEstServ.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrOrdStopsEstServ[centro].getData().getStop_code().intValue()==stopCodeAct.getData().intValue()){
					stopEstServ.add(arrOrdStopsEstServ[centro].getData());
					break;
				}
				else if(stopCodeAct.getData().intValue() < arrOrdStopsEstServ[centro].getData().getStop_code().intValue() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			stopCodeAct = stopCodeAct.getNext();
		}
		System.out.println(5);

		LinkedList<Stops_Estim_ServiceVO> stopEstServRet = new LinkedList<>();
		Node<Stops_Estim_ServiceVO> stESActual = stopEstServ.getHead();
		while(stESActual != null){
			Iterator<ScheduleVO> it = stESActual.getData().getSchedules().iterator();
			while(it.hasNext()){
				ScheduleVO scAct = it.next();
				if(scAct.getScheduleStatus().equals("-")){
					stopEstServRet.add(stESActual.getData());
					break;
				}
			}
			stESActual = stESActual.getNext();
		}
		System.out.println(6);

		Node<Stops_Estim_ServiceVO> stERAct = stopEstServRet.getHead();
		while(stERAct != null){
			Iterator<ScheduleVO> it = stERAct.getData().getSchedules().iterator();
			while(it.hasNext()){
				ScheduleVO scAct = it.next();
				if(scAct.getScheduleStatus().equals("-")&&scAct.getExpectedLeaveTime().endsWith("pm")){
					if(scAct.getExpectedLeaveTime().startsWith("12")){
						h12++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("1")){
						h13++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("2")){
						h14++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("3")){
						h15++;

					}
					else if(scAct.getExpectedLeaveTime().startsWith("4")){
						h16++;

					}
					else if(scAct.getExpectedLeaveTime().startsWith("5")){
						h17++;

					}
					else if(scAct.getExpectedLeaveTime().startsWith("6")){
						h18++;

					}
					else if(scAct.getExpectedLeaveTime().startsWith("7")){
						h19++;

					}
					else if(scAct.getExpectedLeaveTime().startsWith("8")){
						h20++;

					}
					else if(scAct.getExpectedLeaveTime().startsWith("9")){
						h21++;

					}
					else if(scAct.getExpectedLeaveTime().startsWith("10")){
						h22++;

					}
					else if(scAct.getExpectedLeaveTime().startsWith("11")){
						h23++;

					}
				}
				else if(scAct.getScheduleStatus().equals("-") && scAct.getExpectedLeaveTime().endsWith("am")){
					if(scAct.getExpectedLeaveTime().startsWith("0")||scAct.getExpectedLeaveTime().startsWith("12")){
						h0++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("1")){
						h1++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("2")){
						h2++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("3")){
						h3++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("4")){
						h4++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("5")){
						h5++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("6")){
						h6++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("7")){
						h7++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("8")){
						h8++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("9")){
						h9++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("10")){
						h10++;
					}
					else if(scAct.getExpectedLeaveTime().startsWith("11")){
						h11++;
					}
				}
			}
			stERAct = stERAct.getNext();
		}

		LinkedList<MyInteger> lista = new LinkedList<>();
		lista.add(new MyInteger(h0));
		lista.add(new MyInteger(h1));
		lista.add(new MyInteger(h2));
		lista.add(new MyInteger(h3));
		lista.add(new MyInteger(h4));
		lista.add(new MyInteger(h5));
		lista.add(new MyInteger(h6));
		lista.add(new MyInteger(h7));
		lista.add(new MyInteger(h8));
		lista.add(new MyInteger(h9));
		lista.add(new MyInteger(h10));
		lista.add(new MyInteger(h11));
		lista.add(new MyInteger(h12));
		lista.add(new MyInteger(h13));
		lista.add(new MyInteger(h14));
		lista.add(new MyInteger(h15));
		lista.add(new MyInteger(h16));
		lista.add(new MyInteger(h17));
		lista.add(new MyInteger(h18));
		lista.add(new MyInteger(h19));
		lista.add(new MyInteger(h20));
		lista.add(new MyInteger(h21));
		lista.add(new MyInteger(h22));
		lista.add(new MyInteger(h23));

		MyInteger[] arrlistaHoras = (MyInteger[]) listaArreglo(lista);
		arrlistaHoras = radix.sort(arrlistaHoras);
		LinkedList<String> listaDefOrd = new LinkedList<>();
		boolean s0 = false;
		boolean s1 = false;
		boolean s2 = false;
		boolean s3 = false;
		boolean s4 = false;
		boolean s5 = false;
		boolean s6 = false;
		boolean s7= false;
		boolean s8 = false;
		boolean s9 = false;
		boolean s10 = false;
		boolean s11 = false;
		boolean s12 = false;
		boolean s13 = false;
		boolean s14 = false;
		boolean s15 = false;
		boolean s16 = false;
		boolean s17 = false;
		boolean s18 = false;
		boolean s19 = false;
		boolean s20 = false;
		boolean s21 = false;
		boolean s22 = false;
		boolean s23 = false;
		for(int i=0; i<arrlistaHoras.length; i++){

			if(arrlistaHoras[i].intValue() == h0&&!s0){
				listaDefOrd.add(H0A);
				s0 = true;
			}
			else if(arrlistaHoras[i].intValue() == h1 && !s1){
				listaDefOrd.add(H1A);
				s1 = true;
			}
			else if(arrlistaHoras[i].intValue() == h2 && !s2){
				listaDefOrd.add(H2A);
				s2 = true;
			}
			else if(arrlistaHoras[i].intValue() == h3 && !s3){
				listaDefOrd.add(H3A);
				s3 = true;
			}
			else if(arrlistaHoras[i].intValue() == h4 && !s4){
				listaDefOrd.add(H4A);
				s4 = true;
			}
			else if(arrlistaHoras[i].intValue() == h5 && !s5){
				listaDefOrd.add(H5A);
				s5 = true;
			}
			else if(arrlistaHoras[i].intValue() == h6 && !s6){
				listaDefOrd.add(H6A);
				s6 = true;
			}
			else if(arrlistaHoras[i].intValue() == h7 && !s7){
				listaDefOrd.add(H7A);
				s7 = true;
			}
			else if(arrlistaHoras[i].intValue() == h8 && !s8){
				listaDefOrd.add(H8A);
				s8 = true;
			}
			else if(arrlistaHoras[i].intValue() == h9 && !s9){
				listaDefOrd.add(H9A);
				s9 = true;
			}
			else if(arrlistaHoras[i].intValue() == h10 && !s10){
				listaDefOrd.add(H10A);
				s10 = true;
			}
			else if(arrlistaHoras[i].intValue() == h11 && !s11){
				listaDefOrd.add(H11A);
				s11 = true;
			}
			else if(arrlistaHoras[i].intValue() == h12 && !s12){
				listaDefOrd.add(H12A);
				s12 = true;
			}
			else if(arrlistaHoras[i].intValue() == h13 && !s13){
				listaDefOrd.add(H13A);
				s13 = true;
			}
			else if(arrlistaHoras[i].intValue() == h14 && !s14){
				listaDefOrd.add(H14A);
				s14 = true;
			}
			else if(arrlistaHoras[i].intValue() == h15 && !s15){
				listaDefOrd.add(H15A);
				s15 = true;
			}
			else if(arrlistaHoras[i].intValue() == h16 && !s16){
				listaDefOrd.add(H16A);
				s16 = true;
			}
			else if(arrlistaHoras[i].intValue() == h17 && !s17){
				listaDefOrd.add(H17A);
				s17 = true;
			}
			else if(arrlistaHoras[i].intValue() == h18 && !s18){
				listaDefOrd.add(H18A);
				s18 = true;
			}else if(arrlistaHoras[i].intValue() == h19 && !s19){
				listaDefOrd.add(H19A);
				s19 = true;
			}
			else if(arrlistaHoras[i].intValue() == h20 && !s20){
				listaDefOrd.add(H20A);
				s20 = true;
			}
			else if(arrlistaHoras[i].intValue() == h21 && !s21){
				listaDefOrd.add(H21A);
				s21 = true;
			}
			else if(arrlistaHoras[i].intValue() == h22 && !s22){
				listaDefOrd.add(H22A);
				s22 = true;
			}
			else if(arrlistaHoras[i].intValue() == h23 && !s23){
				listaDefOrd.add(H23A);
				s23 = true;
			}
		}

		LinkedList<String> def = new LinkedList<String>(); 
		Node<String> actDef = listaDefOrd.getHead();
		String[] partes = actDef.getData().split("-");
		def.addAtEnd(partes[0]);
		def.addAtEnd(partes[1]);
		String dataDef = actDef.getData();
		if(dataDef.equals(H0A)){
			def.addAtEnd(h0.toString());
		}
		else if(dataDef.equals(H1A)){
			def.addAtEnd(h1.toString());
		}
		else if(dataDef.equals(H2A)){
			def.addAtEnd(h2.toString());
		}
		else if(dataDef.equals(H3A)){
			def.addAtEnd(h3.toString());
		}
		else if(dataDef.equals(H4A)){
			def.addAtEnd(h4.toString());
		}
		else if(dataDef.equals(H5A)){
			def.addAtEnd(h5.toString());
		}
		else if(dataDef.equals(H6A)){
			def.addAtEnd(h6.toString());
		}
		else if(dataDef.equals(H7A)){
			def.addAtEnd(h7.toString());
		}
		else if(dataDef.equals(H8A)){
			def.addAtEnd(h8.toString());
		}
		else if(dataDef.equals(H9A)){
			def.addAtEnd(h9.toString());
		}
		else if(dataDef.equals(H10A)){
			def.addAtEnd(h10.toString());
		}
		else if(dataDef.equals(H11A)){
			def.addAtEnd(h11.toString());
		}
		else if(dataDef.equals(H12A)){
			def.addAtEnd(h12.toString());
		}
		else if(dataDef.equals(H13A)){
			def.addAtEnd(h13.toString());
		}
		else if(dataDef.equals(H14A)){
			def.addAtEnd(h14.toString());
		}
		else if(dataDef.equals(H15A)){
			def.addAtEnd(h15.toString());
		}
		else if(dataDef.equals(H16A)){
			def.addAtEnd(h16.toString());
		}
		else if(dataDef.equals(H17A)){
			def.addAtEnd(h17.toString());
		}
		else if(dataDef.equals(H18A)){
			def.addAtEnd(h18.toString());
		}
		else if(dataDef.equals(H19A)){
			def.addAtEnd(h19.toString());
		}
		else if(dataDef.equals(H20A)){
			def.addAtEnd(h20.toString());
		}
		else if(dataDef.equals(H21A)){
			def.addAtEnd(h21.toString());
		}
		else if(dataDef.equals(H22A)){
			def.addAtEnd(h22.toString());
		}
		else if(dataDef.equals(H23A)){
			def.addAtEnd(h23.toString());
		}


		return def;
	}

	@Override
	public LinkedList<ServiceVO> ITSserviciosMayorDistancia (String fecha){

		LinkedList<TripsVO> listaTrips = tripsEnFechaSolo(Integer.parseInt(fecha));
		LinkedList<MyInteger<TripsVO>> listaIdsT = new LinkedList<>();
		Node<TripsVO> tripAct = listaTrips.getHead();
		while(tripAct != null){
			MyInteger<TripsVO> nuevo = new MyInteger(0, 0); 
			nuevo.setData(tripAct.getData());
			listaIdsT.add(nuevo);
			tripAct = tripAct.getNext();
		}

		MyInteger<Stop_timesVO>[] arrStopTimes = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);
		for(int i =0; i<arrStopTimes.length; i++){
			arrStopTimes[i].setValue(arrStopTimes[i].getData().getTrip_id());
		}
		arrStopTimes = radix.sort(arrStopTimes);

		Node<MyInteger<TripsVO>> myAct = listaIdsT.getHead();
		while(myAct != null){
			int n = arrStopTimes.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrStopTimes[centro].getData().getTrip_id().intValue()==myAct.getData().getData().getTrip_id().intValue()){
					myAct.getData().sumDoubleValue(arrStopTimes[centro].getData().getShape_dist_traveled());
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<arrStopTimes.length && arrStopTimes[mayor].getData() != null  && !s1){
						if(arrStopTimes[mayor].getData().getTrip_id() == myAct.getData().getData().getTrip_id()){
							myAct.getData().sumDoubleValue(arrStopTimes[mayor].getData().getShape_dist_traveled());
							mayor += 1;
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && arrStopTimes[menor].getData() != null && !s2){
						if(arrStopTimes[menor].getData().getTrip_id() == myAct.getData().getData().getTrip_id()){
							myAct.getData().sumDoubleValue(arrStopTimes[menor].getData().getShape_dist_traveled());
							menor -=1;
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(myAct.getData().getData().getTrip_id().intValue() < arrStopTimes[centro].getData().getTrip_id().intValue() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			myAct = myAct.getNext();
		}
		LinkedList<ServiceVO> listaServicios = new LinkedList<>();
		Node<CalendarVO>cAct = calendar.getHead();
		while(cAct != null){
			listaServicios.add(new ServiceVO(cAct.getData().getService_id()));
			cAct = cAct.getNext();
		}

		Node<MyInteger<TripsVO>> myTAct = listaIdsT.getHead();
		while(myTAct != null){
			Node<ServiceVO> servAct = listaServicios.getHead();
			while(servAct != null){
				if(myTAct.getData().getData().getService_id() == servAct.getData().getServiceId().intValue()){
					servAct.getData().sumDist(myTAct.getData().getDoubleValue());
					break;
				}
				servAct = servAct.getNext();
			}
			myTAct = myTAct.getNext();
		}		

		MyInteger<ServiceVO>[] arrServices = (MyInteger<ServiceVO>[]) listaArreglo(listaServicios);
		for(int i=0; i<arrServices.length-1; i++){
			arrServices[i].setValue(arrServices[i].getData().getDistanciaRecorrida().intValue());
		}
		arrServices = radix.sort(arrServices);

		LinkedList<ServiceVO> def = new LinkedList<>();
		for(int i=0; i<arrServices.length; i++){
			def.add(arrServices[i].getData());
		}


		return def;
	}

	public LinkedList<TripsVO> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaIn, String horaFin){

		LinkedList<TripsVO> tripsFecha = tripsEnFechaSolo(Integer.parseInt(fecha));

		MyInteger<Stop_timesVO>[] arrStopTimes = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);
		for(int i =0; i<arrStopTimes.length; i++){
			arrStopTimes[i].setValue(arrStopTimes[i].getData().getTrip_id());
		}
		arrStopTimes = radix.sort(arrStopTimes);

		Integer horaInicial = horaAInt(horaIn);
		Integer horaFinal = horaAInt(horaFin);

		LinkedList<Stop_timesVO> stopTHora = new LinkedList<>();
		Node<TripsVO> tAct = tripsFecha.getHead();
		while(tAct != null){
			int n = arrStopTimes.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrStopTimes[centro].getData().getTrip_id().intValue()==tAct.getData().getTrip_id().intValue()){
					Integer centroHi = horaAInt(arrStopTimes[centro].getData().getArrival_time());
					Integer centroHf = horaAInt(arrStopTimes[centro].getData().getDeparture_time());
					if(centroHi>=horaInicial&&centroHf<=horaFinal){
						stopTHora.add(arrStopTimes[centro].getData());
					}
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<arrStopTimes.length && arrStopTimes[mayor].getData() != null  && !s1){
						if(arrStopTimes[mayor].getData().getTrip_id().intValue() == tAct.getData().getTrip_id().intValue()){
							Integer mayorHi = horaAInt(arrStopTimes[mayor].getData().getArrival_time());
							Integer mayorHf = horaAInt(arrStopTimes[mayor].getData().getDeparture_time());
							if(mayorHi.intValue()>=horaInicial.intValue()&&mayorHf.intValue()<=horaFinal.intValue()){
								stopTHora.add(arrStopTimes[mayor].getData());
							}
							mayor += 1;
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && arrStopTimes[menor].getData() != null && !s2){
						if(arrStopTimes[menor].getData().getTrip_id().intValue() == tAct.getData().getTrip_id().intValue()){
							Integer menorHi = horaAInt(arrStopTimes[menor].getData().getArrival_time());
							Integer menorHf = horaAInt(arrStopTimes[menor].getData().getDeparture_time());
							if(menorHi.intValue()>=horaInicial.intValue()&&menorHf.intValue()<=horaFinal.intValue()){
								stopTHora.add(arrStopTimes[menor].getData());
							}
							menor -=1;
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(tAct.getData().getTrip_id().intValue() < arrStopTimes[centro].getData().getTrip_id().intValue() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			tAct = tAct.getNext();
		}

		MyInteger<Stop_timesVO>[] arrStopTHora = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTHora);
		for(int i =0; i<arrStopTHora.length; i++){
			arrStopTHora[i].setValue(arrStopTHora[i].getData().getTrip_id());
		}
		arrStopTHora = radix.sort(arrStopTHora);

		LinkedList<Stop_timesVO> muyOrd = new LinkedList<>();
		for(int i=0; i<arrStopTHora.length;){
			Stop_timesVO act = arrStopTHora[i].getData();
			MyInteger<Stop_timesVO>[] subLista = new MyInteger[100];
			MyInteger<Stop_timesVO> subi = new MyInteger<>(arrStopTHora[i].getData().getStop_id());
			subi.setData(act);
			subLista[0] = subi;
			int j=i+1;

			int contTam = 1;
			boolean boo = false;
			for(int z = 1; z<subLista.length && j<arrStopTHora.length && !boo; z++,j++){
				Stop_timesVO sig = arrStopTHora[j].getData();
				if(act.getTrip_id().intValue() == sig.getTrip_id().intValue()){
					MyInteger<Stop_timesVO> nuevo = new MyInteger<>(sig.getStop_id().intValue());
					nuevo.setData(sig);
					subLista[z] = nuevo;
					contTam++;
				}
				else{
					boo = true;
				}
			}
			subLista = Arrays.copyOf(subLista, contTam);
			subLista = radix.sort(subLista);

			for(int m=0; m<subLista.length && subLista[m]!= null; m++){
				muyOrd.addAtEnd(subLista[m].getData());
			}
			i=j;
		}

		LinkedList<Integer> idsTripsDef = new LinkedList<>();
		Node<Stop_timesVO> stAct = muyOrd.getHead();
		while(stAct != null){
			Node<Stop_timesVO> sig = stAct.getNext();
			if(sig != null && stAct.getData().getTrip_id().intValue() == sig.getData().getTrip_id().intValue()){
				if(sig.getData().getStop_id().intValue() == stAct.getData().getStop_id().intValue()){
					idsTripsDef.addAtEnd(sig.getData().getTrip_id());
					stAct = stAct.getNext();
				}
				else{
					stAct = stAct.getNext();
				}
			}
			else{
				stAct = stAct.getNext();
			}
		}

		MyInteger<Integer>[] arrIdsTripsDef = (MyInteger<Integer>[]) listaArreglo(idsTripsDef);
		LinkedList<Integer> idsTripsNoR = new LinkedList<>();
		MyInteger<Integer> intAct = arrIdsTripsDef[0]; 
		idsTripsNoR.add(arrIdsTripsDef[0].getData());
		for(int i=0; i<arrIdsTripsDef.length; i++){
			MyInteger<Integer> actual = arrIdsTripsDef[i];
			if(intAct.getData().intValue() != actual.getData().intValue()){
				idsTripsNoR.add(actual.getData());
				intAct = actual;
			}
		}

		LinkedList<TripsVO> tripsDef = new LinkedList<>();
		MyInteger<TripsVO>[] arrTrips = (MyInteger<TripsVO>[]) listaArreglo(trips);
		for(int i = 0; i<arrTrips.length; i++){
			arrTrips[i].setValue(arrTrips[i].getData().getTrip_id().intValue());
		}
		arrTrips = radix.sort(arrTrips);
		Node<Integer> idT = idsTripsNoR.getHead();
		while(idT != null){
			int n = arrTrips.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrTrips[centro].getData().getTrip_id().intValue()==idT.getData().intValue()){
					tripsDef.add(arrTrips[centro].getData());
					break;
				}
				else if(idT.getData().intValue() < arrTrips[centro].getData().getTrip_id().intValue() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			idT = idT.getNext();
		}

		return tripsDef;

	}
	public RoutesVO ITSrutaMenorRetardo(String pFecha)throws Exception{
		Integer fecha = Integer.parseInt(pFecha);
		if(fecha != 20170821 && fecha != 20170822){
			throw new Exception("No hay datos para le fecha ingresada");
		}

		LinkedList<TripsVO> listaTrips = tripsEnFechaSolo(fecha);	
		MyInteger<Stop_timesVO>[] arrStopTimes = (MyInteger<Stop_timesVO>[]) listaArreglo(stopTimes);
		Node<TripsVO> tAct = listaTrips.getHead();
		for(int i = 0; i<arrStopTimes.length; i++){
			arrStopTimes[i].setValue(arrStopTimes[i].getData().getTrip_id());
		}
		arrStopTimes = radix.sort(arrStopTimes);
		LinkedList<MyInteger<Stop_timesVO>> stopsT = new LinkedList<>();
		while(tAct != null){
			int n = arrStopTimes.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrStopTimes[centro].getData().getTrip_id().intValue()==tAct.getData().getTrip_id().intValue()){
					MyInteger<Stop_timesVO> nuevo = new MyInteger(horaAIntSeg(arrStopTimes[centro].getData().getDeparture_time()));
					nuevo.setData(arrStopTimes[centro].getData());
					stopsT.add(nuevo);
					int mayor = centro+1;
					int menor = centro-1;
					boolean s1=false;
					boolean s2 = false;
					while(mayor<arrStopTimes.length && arrStopTimes[mayor].getData() != null  && !s1){
						if(arrStopTimes[mayor].getData().getTrip_id() == tAct.getData().getTrip_id()){
							MyInteger<Stop_timesVO> nuevo2 = new MyInteger(horaAIntSeg(arrStopTimes[mayor].getData().getDeparture_time()));
							nuevo2.setData(arrStopTimes[mayor].getData());
							stopsT.add(nuevo2);
							mayor += 1;
						}
						else{
							s1 = true;
						}
					}
					while(menor>=0 && arrStopTimes[menor].getData() != null && !s2){
						if(arrStopTimes[menor].getData().getTrip_id() == tAct.getData().getTrip_id()){
							MyInteger<Stop_timesVO> nuevo3 = new MyInteger(horaAIntSeg(arrStopTimes[menor].getData().getDeparture_time()));
							nuevo3.setData(arrStopTimes[menor].getData());
							stopsT.add(nuevo3);
							menor -=1;
						}
						else{
							s2=true;
						}
					}
					break;
				}
				else if(tAct.getData().getTrip_id() < arrStopTimes[centro].getData().getTrip_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			tAct = tAct.getNext();
		}
		MyInteger<StopsVO>[] arrStops = (MyInteger<StopsVO>[]) listaArreglo(stops);
		for(int i=0; i<arrStops.length; i++){
			arrStops[i].setValue(arrStops[i].getData().getStop_id());
		}
		arrStops = radix.sort(arrStops);

		Node<MyInteger<Stop_timesVO>> stAct = stopsT.getHead();
		while(stAct != null){
			int n = arrStops.length;
			int centro,inf=0,sup=n-1;
			while(inf<=sup){
				centro=(sup+inf)/2;
				if(arrStops[centro].getData().getStop_id()==stAct.getData().getData().getStop_id().intValue()){
					stAct.getData().setDoubleValue(arrStops[centro].getData().getStop_lat());
					stAct.getData().setDoubleValue2(arrStops[centro].getData().getStop_lon());
					break;
				}
				else if(stAct.getData().getData().getStop_id().intValue() < arrStops[centro].getData().getStop_id() ){
					sup=centro-1;
				}
				else {
					inf=centro+1;
				}
			}
			stAct = stAct.getNext();
		}
		Node<MyInteger<Stop_timesVO>> prueba = stopsT.getHead();
		while(prueba != null){
			System.out.println(prueba.getData().getDoubleValue() + " " +prueba.getData().getDoubleValue2());
			prueba = prueba.getNext();
		}


		MyInteger<Bus_ServiceVO>[] arrBuses = (MyInteger<Bus_ServiceVO>[]) listaArreglo(busesService);
		for(int i =0; i<arrBuses.length; i++){
			arrBuses[i].setValue(arrBuses[i].getData().getTripId().intValue());
		}
		arrBuses = radix.sort(arrBuses);

		/**LinkedList<Bus_ServiceVO> muyOrd = new LinkedList<>();
		for(int i=0; i<arrBuses.length;){
			Stop_timesVO act = arrBuses[i].getData();
			MyInteger<Stop_timesVO>[] subLista = new MyInteger[100];
			MyInteger<Stop_timesVO> subi = new MyInteger<>(arrBuses[i].getData().getStop_id());
			subi.setData(act);
			subLista[0] = subi;
			int j=i+1;

			int contTam = 1;
			boolean boo = false;
			for(int z = 1; z<subLista.length && j<arrBuses.length && !boo; z++,j++){
				Stop_timesVO sig = arrBuses[j].getData();
				if(act.getTrip_id().intValue() == sig.getTrip_id().intValue()){
					MyInteger<Stop_timesVO> nuevo = new MyInteger<>(sig.getStop_id().intValue());
					nuevo.setData(sig);
					subLista[z] = nuevo;
					contTam++;
				}
				else{
					boo = true;
				}
			}
			subLista = Arrays.copyOf(subLista, contTam);
			subLista = radix.sort(subLista);

			for(int m=0; m<subLista.length && subLista[m]!= null; m++){
				muyOrd.addAtEnd(subLista[m].getData());
			}
			i=j;
		}**/

		Node<MyInteger<Stop_timesVO>> act = stopsT.getHead();
		while(act != null){
			int m = arrBuses.length;
			int center,less=0,max=m-1;
			while(less<=max){
				center=(max+less)/2;
				if(arrBuses[center].getData().getTripId().intValue()==act.getData().getData().getTrip_id().intValue()){
					if(arrBuses[center].getData().getLatitude().doubleValue()==act.getData().getDoubleValue() 
							&& arrBuses[center].getData().getLongitude().doubleValue() == act.getData().getDoubleValue2()){
						int horaBus = hora2AIntSeg(arrBuses[center].getData().getRecordedTime());
						if(act.getData().intValue() - horaBus < 0){
							act.getData().setValue2((act.getData().intValue() - horaBus)*-1);
						}
						break;
					}
					boolean si1=false;
					boolean si2 = false;
					int mayor = center+1;
					int menor = center-1;
					while(mayor<arrBuses.length && arrBuses[mayor].getData() != null && arrBuses[mayor].getData().getTripId().intValue()==act.getData().getData().getTrip_id().intValue()){
						if(arrBuses[mayor].getData().getLatitude().doubleValue()==act.getData().getDoubleValue() 
								&& arrBuses[mayor].getData().getLongitude().doubleValue() == act.getData().getDoubleValue2()){
							si1=true;
							int horaBus = hora2AIntSeg(arrBuses[mayor].getData().getRecordedTime());
							if(act.getData().intValue() - horaBus <0){
								act.getData().setValue2((act.getData().intValue()-horaBus)*-1);
							}
							break;
						}
						mayor += 1;
					}
					if(si1==true){break;}
					while(menor>=0 && arrBuses[menor].getData() != null && arrBuses[menor].getData().getTripId().intValue()==act.getData().getData().getTrip_id().intValue()){
						if(arrBuses[menor].getData().getLatitude().doubleValue()==act.getData().getDoubleValue() 
								&& arrBuses[menor].getData().getLongitude().doubleValue() == act.getData().getDoubleValue2()){
							si2 = true;
							int horaBus = hora2AIntSeg(arrBuses[menor].getData().getRecordedTime());
							if(act.getData().intValue() - horaBus <0){
								act.getData().setValue2((act.getData().intValue()-horaBus)*-1);
							}
							break;
						}
						menor -= 1;
					}
					if(si2==true){break;}
					break;
				}
				else if(act.getData().getData().getTrip_id().intValue() < arrBuses[center].getData().getTripId().intValue() ){
					max=center-1;
				}
				else {
					less=center+1;
				}
			}
			act = act.getNext();
		}
		Node<MyInteger<Stop_timesVO>> stopAct = stopsT.getHead();
		MyInteger<Stop_timesVO>[] arrStopsT = (MyInteger<Stop_timesVO>[]) listaMyIntegerArreglo(stopsT);
		for(int i=0; i<arrStopsT.length; i++){
			arrStopsT[i].setValue(arrStopsT[i].getValue2());
		}
		arrStopsT = radix.sort(arrStopsT);
		for(int i =0;i<arrStopsT.length; i++){
			System.out.println(arrStopsT[i].getValue());
		}
		return null;
	}
	//----------------------------COMPLEMENTOS DE METODOS--------------------//

	public MyInteger<T>[] listaArreglo(LinkedList pLista){

		MyInteger<T>[] arr = new MyInteger[pLista.getSize()];
		Node<T> act = pLista.getHead();

		for(int i = 0; i<pLista.getSize()&& act != null; i++){
			MyInteger<T> n = new MyInteger<>(i);
			n.setData(act.getData());
			arr[i] = n;
			act = act.getNext();
		}

		return arr;

	}
	public MyInteger<T>[] listaMyIntegerArreglo(LinkedList pLista){

		MyInteger<T>[] arr = new MyInteger[pLista.getSize()];
		Node<MyInteger<T>> act = pLista.getHead();

		for(int i = 0; i<pLista.getSize()&& act != null; i++){
			MyInteger<T> n = new MyInteger<>(0,0);
			n.setData(act.getData().getData());
			n.setValue(act.getData().intValue());
			n.setValue2(act.getData().getValue2());
			arr[i] = n;
			act = act.getNext();
		}

		return arr;

	}
	public LinkedList<TripsVO> tripsEnFecha(int pRuta, int fecha){
		LinkedList<TripsVO> lista = new LinkedList<>();
		LinkedList<TripsVO> lTrips = new LinkedList<>();
		Node<TripsVO> tAct = trips.getHead();
		boolean t= false;
		while(tAct != null ){
			if(tAct.getData().getRoute_id() == pRuta){
				t = true;
				lTrips.add(tAct.getData());
			}
			else if(t == true && !lTrips.isEmpty()){
				break;
			}
			tAct = tAct.getNext();
		}
		Node<TripsVO> mAct = lTrips.getHead();
		while(mAct != null){
			Node<CalendarVO> cAct = calendar.getHead();
			while(cAct != null){
				if(cAct.getData().getService_id() == mAct.getData().getService_id()){
					boolean si = false;
					int dia = getDay(intADate(fecha));
					if(dia == 1){
						if(cAct.getData().getSunday() == 0){lista.add(mAct.getData());}
						break;
					}
					else if(dia == 2){
						if(cAct.getData().getMonday() == 0){lista.add(mAct.getData());}
						break;
					}
					else if(dia == 3){
						if(cAct.getData().getTuesday() == 0){lista.add(mAct.getData());}
						break;
					}
					else if(dia == 4){
						if(cAct.getData().getWednesday() == 0){lista.add(mAct.getData());}
						break;
					}
					else if(dia == 2){
						if(cAct.getData().getThursday() == 0){lista.add(mAct.getData());}
						break;
					}
					else if(dia == 2){
						if(cAct.getData().getFriday() == 0){lista.add(mAct.getData());}
						break;
					}
					else{
						if(cAct.getData().getSaturday() == 0){lista.add(mAct.getData());
						break;
						}

					}
				}
				cAct = cAct.getNext();
			}
			mAct = mAct.getNext();
		}
		return lista;
	}

	public Date intADate(Integer fecha){
		Date date = null;
		SimpleDateFormat originalFormat = new SimpleDateFormat("yyyyMMdd");
		try {
			date = originalFormat.parse(fecha.toString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public int getDay(Date d){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(d);
		return cal.get(Calendar.DAY_OF_WEEK);	
	}

	public LinkedList<TripsVO> tripsEnFechaSolo(int fecha){
		LinkedList<TripsVO> lista = new LinkedList<>();

		Node<TripsVO> mAct = trips.getHead();
		while(mAct != null){
			Node<CalendarVO> cAct = calendar.getHead();
			while(cAct != null){
				if(cAct.getData().getService_id() == mAct.getData().getService_id()){
					boolean si = false;
					int dia = getDay(intADate(fecha));
					if(dia == 1){
						if(cAct.getData().getSunday() == 0){lista.add(mAct.getData());}
						break;
					}
					else if(dia == 2){
						if(cAct.getData().getMonday() == 0){lista.add(mAct.getData());}
						break;
					}
					else if(dia == 3){
						if(cAct.getData().getTuesday() == 0){lista.add(mAct.getData());}
						break;
					}
					else if(dia == 4){
						if(cAct.getData().getWednesday() == 0){lista.add(mAct.getData());}
						break;
					}
					else if(dia == 2){
						if(cAct.getData().getThursday() == 0){lista.add(mAct.getData());}
						break;
					}
					else if(dia == 2){
						if(cAct.getData().getFriday() == 0){lista.add(mAct.getData());}
						break;
					}
					else{
						if(cAct.getData().getSaturday() == 0){lista.add(mAct.getData());
						break;
						}

					}
				}
				cAct = cAct.getNext();
			}
			mAct = mAct.getNext();
		}
		return lista;
	}

	public Integer horaAInt(String hora){
		hora = hora.replaceAll("\\s+","");
		String[] partesH = hora.split(":");
		Integer horaA = Integer.parseInt(partesH[0])*10000;
		Integer min = Integer.parseInt(partesH[1])*100;
		Integer seg = Integer.parseInt(partesH[2]);
		Integer intHora = horaA+min+seg;

		return intHora;
	}

	public boolean estaEnLista(Integer dato, LinkedList<Integer> lista){
		boolean si=false;

		Node<Integer> actual = lista.getHead();
		while(actual != null && !si){
			if(actual.getData().intValue() == dato.intValue()){
				si = true;
				break;
			}
			actual = actual.getNext();
		}
		return si;
	}
	public Integer horaAIntSeg(String hora){
		hora = hora.replaceAll("\\s+","");
		String[] partesH = hora.split(":");
		Integer horaA = Integer.parseInt(partesH[0])*3600;
		Integer min = Integer.parseInt(partesH[1])*60;
		Integer seg = Integer.parseInt(partesH[2]);
		Integer intHora = horaA+min+seg;

		return intHora;
	}

	public Integer hora2AIntSeg(String hora){
		String[] partes = hora.split(":");
		String[] parte3 = partes[2].split("\\s+");

		Integer horaA = Integer.parseInt(partes[0]);
		if(parte3[1].equals("pm")){
			horaA += 12;
		}
		horaA = horaA*3600;
		Integer min = Integer.parseInt(partes[1])*60;
		Integer seg = Integer.parseInt(parte3[0]);
		Integer intHora = horaA+min+seg;

		return intHora;
	}

}
