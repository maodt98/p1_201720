package model.vo;

public class Calendar_datesVO {

	private int service_id;
	private int date;
 	private byte exception_type;
 	
	public Calendar_datesVO(int service_id, int date, byte exception_type) {
		super();
		this.service_id = service_id;
		this.date = date;
		this.exception_type = exception_type;
	}

	public int getService_id() {
		return service_id;
	}

	public void setService_id(int service_id) {
		this.service_id = service_id;
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public byte getException_type() {
		return exception_type;
	}

	public void setException_type(byte exception_type) {
		this.exception_type = exception_type;
	}
	
}
