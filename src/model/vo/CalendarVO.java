package model.vo;

public class CalendarVO {

	private Integer service_id;
 	private byte monday;
	private byte tuesday;
	private byte wednesday;
	private byte thursday;
	private byte friday;
	private byte saturday;
	private byte sunday; 
	private int start_date;
	private int end_date;
	
	public CalendarVO(Integer service_id, byte monday, byte tuesday, byte wednesday, byte thursday, byte friday,
			byte saturday, byte sunday, int start_date, int end_date) {
		super();
		this.service_id = service_id;
		this.monday = monday;
		this.tuesday = tuesday;
		this.wednesday = wednesday;
		this.thursday = thursday;
		this.friday = friday;
		this.saturday = saturday;
		this.sunday = sunday;
		this.start_date = start_date;
		this.end_date = end_date;
	}

	public int getService_id() {
		return service_id;
	}

	public void setService_id(Integer service_id) {
		this.service_id = service_id;
	}

	public byte getMonday() {
		return monday;
	}

	public void setMonday(byte monday) {
		this.monday = monday;
	}

	public byte getTuesday() {
		return tuesday;
	}

	public void setTuesday(byte tuesday) {
		this.tuesday = tuesday;
	}

	public byte getWednesday() {
		return wednesday;
	}

	public void setWednesday(byte wednesday) {
		this.wednesday = wednesday;
	}

	public byte getThursday() {
		return thursday;
	}

	public void setThursday(byte thursday) {
		this.thursday = thursday;
	}

	public byte getFriday() {
		return friday;
	}

	public void setFriday(byte friday) {
		this.friday = friday;
	}

	public byte getSaturday() {
		return saturday;
	}

	public void setSaturday(byte saturday) {
		this.saturday = saturday;
	}

	public byte getSunday() {
		return sunday;
	}

	public void setSunday(byte sunday) {
		this.sunday = sunday;
	}

	public int getStart_date() {
		return start_date;
	}

	public void setStart_date(int start_date) {
		this.start_date = start_date;
	}

	public int getEnd_date() {
		return end_date;
	}

	public void setEnd_date(int end_date) {
		this.end_date = end_date;
	}
}
