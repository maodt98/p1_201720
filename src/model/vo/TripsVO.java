package model.vo;

public class TripsVO {

	private Integer route_id;
	private int service_id;
	private Integer trip_id;
	private String trip_headsign;
	private String trip_short_name;
	private byte direction_id;
	private int block_id;
 	private int shape_id;
	private byte wheelchair_accessible;
	private byte bikes_allowed;
	
	public TripsVO(int route_id, int agency_id, int trip_id, String trip_headsign, String trip_short_name,
			byte direction_id, int block_id, int shape_id, byte wheelchair_accessible, byte bikes_allowed) {
		super();
		this.route_id = route_id;
		this.service_id = agency_id;
		this.trip_id = trip_id;
		this.trip_headsign = trip_headsign;
		this.trip_short_name = trip_short_name;
		this.direction_id = direction_id;
		this.block_id = block_id;
		this.shape_id = shape_id;
		this.wheelchair_accessible = wheelchair_accessible;
		this.bikes_allowed = bikes_allowed;
	}

	public Integer getRoute_id() {
		return route_id;
	}

	public void setRoute_id(int route_id) {
		this.route_id = route_id;
	}

	public int getService_id() {
		return service_id;
	}

	public void setService_id(int service_id) {
		this.service_id = service_id;
	}

	public Integer getTrip_id() {
		return trip_id;
	}

	public void setTrip_id(int trip_id) {
		this.trip_id = trip_id;
	}

	public String getTrip_headsign() {
		return trip_headsign;
	}

	public void setTrip_headsign(String trip_headsign) {
		this.trip_headsign = trip_headsign;
	}

	public String getTrip_short_name() {
		return trip_short_name;
	}

	public void setTrip_short_name(String trip_short_name) {
		this.trip_short_name = trip_short_name;
	}

	public byte getDirection_id() {
		return direction_id;
	}

	public void setDirection_id(byte direction_id) {
		this.direction_id = direction_id;
	}

	public int getBlock_id() {
		return block_id;
	}

	public void setBlock_id(int block_id) {
		this.block_id = block_id;
	}

	public int getShape_id() {
		return shape_id;
	}

	public void setShape_id(int shape_id) {
		this.shape_id = shape_id;
	}

	public byte getWheelchair_accessible() {
		return wheelchair_accessible;
	}

	public void setWheelchair_accessible(byte wheelchair_accessible) {
		this.wheelchair_accessible = wheelchair_accessible;
	}

	public byte getBikes_allowed() {
		return bikes_allowed;
	}

	public void setBikes_allowed(byte bikes_allowed) {
		this.bikes_allowed = bikes_allowed;
	}
	
	/**
	 * Retorna el id de la ruta perteneciente al servicio dado por par�metro
	 * @param Servicio del que se quiere saber la ruta
	 * @return Ruta segun el servicio
	 */
	public int getRoute(int service)
	{
		int retornar = 0;
		if(service==service_id)
		{
			retornar = route_id;
		}
		return retornar;
	}
	public TripsVO darTripRutaServicio(Integer pRuta, Integer pServicio)
	{
		TripsVO retornar = null;
		if(pRuta == this.getRoute(pServicio))
		{
			retornar = this;
		}
		return retornar;
	}
	/**
	 * Dice si hay una ruta asociada al n�mero de servicio y id de ruta requerido
	 * @param pRuta
	 * @param pServicio
	 * @return
	 */
	public boolean darRutaServicio(Integer pRuta, Integer pServicio)
	{
		boolean rta = false;
		if(pRuta==route_id && pServicio ==service_id)
			
		{
			rta= true;
		}
		return rta;
	}
	
}
