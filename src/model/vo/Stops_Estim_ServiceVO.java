package model.vo;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stops_Estim_ServiceVO {

	@SerializedName("RouteNo")
	@Expose
	private String routeNo;
	@SerializedName("RouteName")
	@Expose
	private String routeName;
	@SerializedName("Direction")
	@Expose
	private String direction;
	@SerializedName("RouteMap")
	@Expose
	private RouteMapVO routeMap;
	@SerializedName("Schedules")
	@Expose
	private List<ScheduleVO> schedules = null;
	
	private Integer stop_code;


	public Stops_Estim_ServiceVO(String routeNo, String routeName, String direction, RouteMapVO routeMap, List<ScheduleVO> schedules, Integer pStopCode) {
		super();
		this.routeNo = routeNo;
		this.routeName = routeName;
		this.direction = direction;
		this.routeMap = routeMap;
		this.schedules = schedules;
		this.stop_code = pStopCode;
	}

	public String getRouteNo() {
		return routeNo;
	}

	public void setRouteNo(String routeNo) {
		this.routeNo = routeNo;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public RouteMapVO getRouteMap() {
		return routeMap;
	}

	public void setRouteMap(RouteMapVO routeMap) {
		this.routeMap = routeMap;
	}

	public List<ScheduleVO> getSchedules() {
		return schedules;
	}

	public void setSchedules(List<ScheduleVO> schedules) {
		this.schedules = schedules;
	}

	/**
	 * @return the stop_code
	 */
	public Integer getStop_code() {
		return stop_code;
	}

	/**
	 * @param stop_code the stop_code to set
	 */
	public void setStop_code(Integer stop_code) {
		this.stop_code = stop_code;
	}
}
