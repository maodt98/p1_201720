package model.data_structures;

public class Node<T> {
    /**
     * Nodo siguiente al actual
     */
    private Node<T> next;
    
    private T data;

    /**
     * Metodo constructor de la clase nodo
     */
    public Node() {
		next = null;
		setData(null);
	}

	 public Node(T data) {

		this(data, null);

	}

	 public Node(T data, Node<T> next) {
		this.setData(data);
		this.next = next;
	}

    /**
     * Retorna el siguiente elemento
     * @return Siguiente elemento
     */
    public Node<T> getNext()

    {
        return next;
    }
    //------------------------------------------------------------------------------------------------------------------
    //  Modificadores
    //------------------------------------------------------------------------------------------------------------------
    /**
     * Cambia el siguiente nodo del actual
     * @param nw Nuevo nodo
     */
    public void setNext(Node<T> nw)
    {
        next = nw;
    }

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}

}
