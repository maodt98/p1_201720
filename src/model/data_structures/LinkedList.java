package model.data_structures;

public class LinkedList<T> implements IList<T>
{
	/**
	 * Cabeza de la lista
	 */
	private Node<T> head;
	/**
	 * Ultimo elemento de la lista
	 */
	private Node<T> last;
	/**
	 * Tamanio de la lista
	 */
	private int size;
	/**
	 * Nodo actual
	 */
	private Node<T> current;
	/**
	 * Siguiente nodo del actual
	 */
	private Node<T> next;


	public LinkedList(){

		head = null;
		last = null;
		current = null;
		next = null;
		size = 0;
	}

	/**
	 * Retorna el tamanio de la lista
	 * @return Tamanio de la lista
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * Dice si la lista esta vacia
	 */
	public boolean isEmpty() {
		boolean ans = false;
		if(head==null){
			ans = true;
		}
		return ans;
	}

	/**
	 * Retorna la cabeza de la lista
	 * @return Cabeza de la lista
	 */
	public Node<T> getHead()  {
		return head;
	}

	/**
	 * Agrega un elemento al inicio de la lista.
	 * @param element Nodo a agregar
	 */
	public void add(T element) {
		Node<T> nodo = new Node<>(element);
		if(head == null)
		{
			head = nodo;
			last = nodo;
			last.setNext(null);
			size++;
		}
		else
		{
			nodo.setNext(head);
			head = nodo;
			size++;
		}

	}

	/**
	 * Agrega un elemento al final de la lista
	 * @param element nodo a agregar
	 */
	public void addAtEnd(T element) {

		Node<T> nodo = new Node<>(element);
		if(head == null)
		{
			head = nodo;
			last = nodo;
			last.setNext(null);
			size++;
		}
		else
		{
			last.setNext(nodo);
			last = nodo;
			size++;
		}

	}

	/**
	 * Agrega un elemento en la posicion ingresada por parametro
	 * @param element elemento a agregar
	 * @param pos posicion a donde quiero agregarlo
	 */
	public void addAtk(T element, int pos) throws Exception
	{
		Node<T> nodo = new Node<>(element);
		Integer num = 0;
		current = head;

		if(pos==0)
		{
			add(element);
		}
		else if(pos == size)
		{
			addAtEnd(element);
		}
		else
		{   while(next()!=null)
		{
			current = next;
			num++;
			if (num == pos) {
				current.setNext(nodo);
				nodo.setNext(next());
				size++;
			}
			else
			{
				throw new Exception("No existe la posicion deseada");
			}
		}
		}


	}

	/**
	 * Busca y retorna el elemento que se busca por parametro
	 * @param element Elemento que se quiere buscar
	 * @return Elemento que se encontre
	 */
	public Node<T> getElement(Node<T> element) throws Exception
	{
		Node<T> ret = null;
		current = head;
		if(current == element)
		{
			ret = current;
		}
		else
		{
			current = next();
		}
		if(ret==null)
		{
			throw new Exception("No existe el elemento que se busca");
		}
		return ret;
	}


	/**
	 * Retorna el elemento actual
	 * @return Elemento actual
	 */
	public Node<T> getCurrent()
	{
		return current;
	}

	/**
	 * Elimina un elemento de la lista
	 * @param element nodo a eliminar
	 */
	public void delete(Node<T> element)  throws Exception
	{
		current = head;
		Node<T> del = element;
		Node<T> prev = getPrevious(element);
		if(!isEmpty())
		{
			if(del!=null) {
				if (element == head) {
					head = next();
					size--;
				} else if (element == last) {
					last = prev;
					last.setNext(null);
					size--;
				} else {
					prev.setNext(del.getNext());
					size--;
				}
			}
			else
			{
				throw new Exception("No existe el elemento que desea eliminar");
			}
		}
		else
		{
			throw new Exception("La lista se encuentra vacia");
		}
	}

	/**
	 * Retorna el nodo previo al que se da por parámetro
	 * @param node Nodo Nodo del que se desea buscar el anterior
	 * @return Nodo anterior
	 */
	private Node<T> getPrevious(Node<T> node)
	{
		Node<T> ret = null;
		current = head;
		while(next()!=null)
		{
			if(next().getNext()==node)
			{
				ret= current;
			}
		}
		return ret;
	}


	/**
	 * Retorna el nodo siguiente del actual.
	 * @return Nodo siguiente
	 */
	public Node<T> next() {
		return current.getNext();
	}

	@Override
	public Node<T> getLast() {
		return last;
	}
	/**
	 * Saca y retorna la cabeza de la lista.
	 * @return Cabeza de la lista.
	 */
	public Node<T> getHeadDelete()
	{
		Node<T> retornar = head;
		Node<T> siguiente = head.getNext();
		head = siguiente;
		size--;
		return head;
	}



}
