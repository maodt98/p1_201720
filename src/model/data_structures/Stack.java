package model.data_structures;

public class Stack<T> implements  IList<T>
{

    /**
     * Cabeza de la lista
     */
    private Node<T> head;
    /**
     * Ultimo elemento de la lista
     */
    private Node<T> last;
    /**
     * Tamanio de la lista
     */
    private int size;
    /**
     * Nodo actual
     */
    private Node<T> current;
    /**
     * Siguiente nodo del actual
     */
    private Node<T> next;

//--------------------------------------------------------------------------------------------------------------------
// Metodos
// -------------------------------------------------------------------------------------------------------------------

    /**
     * Metodo constructos de la clase stack
     */
    public Stack()
    {
        head = null;
        last = null;
        current = null;
        next = null;
        size = 0;
    }

    /**
     * Retorna la cabeza de la lista
     * @return Cabeza de la lista
     * @throws  Exception Excepcion si la lista se encuentra vacia
     */
    public Node<T> getHead() {
        Node<T> ret = null;
        if(!isEmpty())
        {
            ret = head;
        }
        return ret;
    }

    /**
     * Retorna el ultimo elemento de la lista
     * @return Ultimo elemento de la lista
     */
    public Node<T> getLast()
    {
        Node<T> ret = null;
        if(!isEmpty())
        {
            ret = last;
        }
        return ret;
    }
    /**
     * Retorna el elemento actual
     * @return Elemento actual
     */
    public Node<T> getCurrent()

    {
        return current;
    }

    /**
     * Agrega un elemento al final de la pila.
     * @param element Nodo a agregar
     */
    public void push(Node<T> element) throws Exception
    {
        current = head;
        if(element!=null) {
            if (head == null) {
                head = element;
                head.setNext(null);
                size++;
                last = element;
            } else {
                last.setNext(element);
                last = element;
                size++;
            }
        }
        else
        {
            throw new Exception("No se puede agregar null a la lista");
        }

    }

    /**
     * Retorna el tamanio de la lista
     * @return Tamanio de la lista
     */
    public Integer getSize() {
        return size;
    }
    /**
     * Busca y retorna el elemento que se busca por parametro
     * @param nodo Elemento que se quiere buscar
     * @return Elemento que se encontre
     */
    public Node<T> getElement(Node<T> nodo) throws Exception
    {
        Node<T> ret = null;
        current = head;
        if(current == nodo)
        {
            ret = current;
        }
        else
        {
            current = next();
        }
        if(ret==null)
        {
            throw new Exception("No existe el elemento que se busca");
        }
        return ret;
    }
    /**
     * Elimina un elemento de la lista
     * @param element nodo a eliminar
     */
    public Node<T> pop()  throws Exception
    {
        current = head;
        Node<T> ret= null;
        Node<T> prev = null;
        if(!isEmpty())
        {

            while(next()!=null)
                {
                  if(current.getNext()==last)
                   {
                	  prev = current;
                   }
               }
               ret = last;
               last = prev;
            }
          
        else
        {
            throw new Exception("La lista se encuentra vacia");
        }
        return ret;
        
    }
      

    /**
     * Dice si la lista esta vacia
     */
    public boolean isEmpty() {
        boolean ans = false;
        if(head==null){
            ans = true;
        }
        return ans;
    }

    /**
     * Retorna el nodo siguiente del actual.
     * @return Nodo siguiente
     */
    public Node<T> next() {
        return current.getNext();
    }



}
