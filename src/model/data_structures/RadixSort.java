package model.data_structures;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import model.logic.STSManager;
import model.vo.RoutesVO;
import model.vo.TransfersVO;

public class RadixSort<T>
{
	private static LinkedList<TransfersVO> transfers = new LinkedList<>();

	// Organiza los numeros comenzando con el digito menos significativo.

	public static MyInteger[] sort(MyInteger[] input){

		for(int place=1; place <= maxDigit(input); place *= 10){

			// Utiliza el CountingSort en cada place
			input = countingSort(input, place);
		}

		return input;
	}

	private static  int maxDigit(MyInteger[] l){
		int max = 0;
		for(int i = 0; i< l.length; i++){
			if(l[i].intValue() > max){
				max = l[i].intValue();
			}
		}
		return max;
	}


	private static  MyInteger[] countingSort(MyInteger[] input, int place){
		MyInteger[] out = new MyInteger[input.length];

		int[] count = new int[10];

		for(int i=0; i < input.length; i++){
			int digit = getDigit(input[i].intValue(), place);
			count[digit] += 1;

		}

		for(int i=1; i < count.length; i++){
			count[i] += count[i-1];
		}

		for(int i = input.length-1; i >= 0; i--){
			int digit = getDigit(input[i].intValue(), place);

			out[count[digit]-1] = input[i];
			count[digit]--;
		}

		return out;

	}

	private  static int getDigit(int value, int digitPlace){
		return ((value/digitPlace ) % 10);
	}

	public static void loadTransfers() {
		String file = "./data/transfers.txt";
		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		try {
			br = new BufferedReader(new FileReader(file));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] datos = line.split(splitBy);
				TransfersVO x = new TransfersVO(Integer.parseInt(datos[0]), Integer.parseInt(datos[1]), Byte.parseByte(datos[2]), (datos.length>3)?Short.parseShort(datos[3]):0);
				transfers.add(x);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**public static void main(String[] args)
	{
		loadTransfers();
		//long start = System.currentTimeMillis();
		//int max = maxDigit(test);
		//long end = System.currentTimeMillis();
		MyInteger<TransfersVO>[] lista = new MyInteger[transfers.getSize()];
		//Integer[] lista = new Integer[transfers.getSize()];
		Node<TransfersVO> act = transfers.getHead();
		int cont = 0;
		while(act != null){
			MyInteger<TransfersVO> i = new MyInteger<TransfersVO>(act.getData().getFrom_stop_id());
			lista[cont] = i;
			i.setData(act.getData());
			cont++;
			act = act.getNext();
			//System.out.println(i);
		}
		lista = sort(lista);

		/**for (MyInteger i : lista){
			System.out.println(i);
		}
		for(int i = 0; i<lista.length; i++){

			System.out.println(lista[i].getData().getFrom_stop_id());
		}
		System.out.println(lista.length);
		//System.out.println(max);
		//System.out.println(end-start);
	}**/
}