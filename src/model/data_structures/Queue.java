package model.data_structures;

public class Queue<T> implements IList<T>
{
    /**
     * Representa al primer nodo de la cola.
     */
    private Node<T> head;
    /**
     * Representa al último nodo de la cola
     */
  private Node<T> last;
    /**
     * Representa al nodo actual de la cola.
     */
  private Node<T> current;
    /**
     * Representa el tamaño de la cola
     */
  private Integer size;
//-------------------------------------------------------------------------------------------------------------------
// Metodos
// ------------------------------------------------------------------------------------------------------------------------

    /**
     * Método constructor de la clase Queue
     */
  public Queue()
  {
      head = null;
      last = null;
  }

    /**
     * Retorna la cabeza de la cola.
     * @return Cabeza de la cola.
     */
  public Node<T> getHead()
  {
      return head;
  }

    /**
     * Retorna el último elemento de la cola.
     * @return Elemento de la cola.
     */
  public Node<T> getLast()
  {
      return last;
  }

    /**
     * Retorna el nodo actual.
     * @return Nodo actual
     */
  public Node<T> getCurrent()
  {
      return current;
  }

    /**
     * Retorna el número de objetos de la cola
     * @return Número de objetos de la cola.
     */
  public Integer getSize()
  {
      return size;
  }

    /**
     * Agrega un elemento en la última posición de la cola
     * @param element Elemento que se quiere añadir a la cola
     * @throws Exception Excepcion si la cola esta vacia
     */
    public void enqueue(Node<T> element)
    {

        if(head ==null)
        {
            head = element;
            head.setNext(null);
            last = element;
            last.setNext(null);
            size++;
        }
        else
        {
            current=head;
            if(next()==null)
            {
                current.setNext(element);
                last = next();
                size++;
            }
            else
            {
                current = next();
            }
        }
    }

    /**
     * Busca y retorna el elemento que se busca por parametro
     * @param element Elemento que se quiere buscar
     * @return Elemento que se encontre
     */
    public Node<T> getElement(Node<T> element) throws Exception
    {
        Node<T> ret = null;
        current = head;
        if(current == element)
        {
            ret = current;
        }
        else
        {
            current = next();
        }
        if(ret==null)
        {
            throw new Exception("No existe el elemento que se busca");
        }
        return ret;
    }

    /**
     * Elimina y retorna el primer elemento que se aniadio a la cola
     * @return Elemento que se elimino
     * @throws Exception Si la cola esta vacia
     */
    public Node<T> dequeue() throws Exception
    {
        Node<T> ret = null;
        Node<T> prev = null;
        if(!isEmpty())
        {
           ret = head;
        	head = head.getNext();
           size--;
        }
        else
        {
            throw  new Exception("La lista se encuentra vacia");
        }
        return ret;
    }

    /**
     * Dice si la cola esta vacia
     * @return True si la cola esta vacia
     * @throws Exception Si la cola esta vacia
     */
  public boolean isEmpty() throws Exception
  {
      boolean ret = false;
      if (head == null)
      {
          ret =true;
          throw new Exception("La cola se encuentra vacia");
      }
    return ret;
  }

    /**
     * Retorna el nodo siguiente al actual
     * @return Siguiente al actual
     */
  public Node<T> next()
  {
      return current.getNext();
  }
}
