package model.data_structures;

public class RadixSortLinked extends LinkedList
{
	/**
	 * Arreglo que contiene las listas que se van a organizar
	 */
	private static LinkedList[] q = null;
	/**
	 * A�ade listas al arreglo
	 * @param lista Lista que se quiere agregar
	 */
	public void addLinked(LinkedList lista1)
	{
		for (int i = 0; i< q.length; i++)
		{
			if(q[i]==null)
			{
				q[i] =lista1;
			}
		}
	}
	 /**
	   * Organiza un arreglo de listas
	   * @param list  La lista desordenada.
	   * @return La lista ordenada.
	   */
	  public static Object[] sort(Object[] list)
	  {
	    // Saca el n�mero m�ximo de d�gitos.
	    int maxDigits = getMaxDigits(list);
	 
	    // Recorre dentro del radix dependiendo del n�mero m�ximo de digitos.
	    for(int r=1; r <= maxDigits; r++){
	 
	      // Recorre entre cada n�mero.
	      int radix = 0;
	      for(int n=0; n < list.length; n++){
	        // Mira cual lista agregar.
	        radix = getDigitAt(Integer.parseInt(list[n].toString()), r);
	        // Put it into it's queue accordinmaxDigits = getMaxDigits(list);g to the radix.
	        q[radix].addAtEnd(list[n]);
	      }
	 
	      // Go through the queues and put the numbers back into the list.
	      int a=0;
	      for(int k=0; k < q.length; k++){
	        // Go through every element in the queue.
	        while(q[k].getHead() != null){
	          list[a++] = q[k].getHeadDelete();
	        }
	      }
	 
	    }
	 
	    // Return the list, it is now sorted.
	    return list;
	 
	  }
	 
	  /**
	   * Gets the maximum digits of a list of integers.
	   * @param list
	   * @return
	   */
	  public static int getMaxDigits(Object list[])
	  {
	    // Define the max digits.
	    int maxDigits = 0;
	 
	    // Iterate through the list.
	    int digits;
	    for(int i=0; i < list.length; i++){
	 
	      // Cast the number to a string.
	      digits = getDigits(Integer.parseInt(list[i].toString()));
	 
	      // Compare the lengths.
	      if(digits > maxDigits){
	        maxDigits = digits;
	      }
	 
	    }
	 
	    // Return the max digits.
	    return maxDigits;
	  }
	 
	  /**
	   * Gets the number of digits the specified number has.
	   * @param i
	   * @return
	   */
	  public static int getDigits(int i)
	  {
	    if(i < 10){
	      return 1;
	    }
	    return 1 + getDigits(i / 10);
	  }
	 
	  /**
	   * Gets the digit at the specified radix of the specified number.
	   * @param number
	   * @param radix
	   * @return
	   */
	  public static int getDigitAt(int number, int radix)
	  {
	    return (int)(number / Math.pow(10,radix-1)) % 10;
	  }
}
