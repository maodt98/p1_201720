package controller;

import api.ISTSManager;
import model.data_structures.IList;
import model.data_structures.LinkedList;
import model.data_structures.MyInteger;
import model.logic.STSManager;
import model.vo.RoutesVO;
import model.vo.ServiceVO;
import model.vo.Stop_timesVO;
import model.vo.StopsVO;
import model.vo.TripsVO;

public class Controller {

	private static ISTSManager  manager = new STSManager();

	public static void ITScargarTR(String pFecha){
		manager.ITScargarTR(pFecha);
	}
	
	public static LinkedList<RoutesVO> ITSrutasPorEmpresa(Integer nFecha, String nEmpresa)throws Exception
	{
		return manager.ITSrutasPorEmpresa( nFecha,  nEmpresa);
	}
	public static LinkedList<TripsVO> ITSviajesRetrasadosRuta(Integer idRuta, Integer nFecha)throws Exception
	{
		return manager.ITSviajesRetrasadosRuta( idRuta,  nFecha);
	}
	public static LinkedList<StopsVO> ITSparadasRetrasadasFecha(Integer nFecha)throws Exception
	{
		return manager.ITSparadasRetrasadasFecha( nFecha);
	}
	public static LinkedList<Stop_timesVO> ITStransbordosRuta(Integer nFecha, Integer idRuta) throws Exception
	{
		return manager.ITStransbordosRuta(nFecha, idRuta);
	}
	public static LinkedList<TripsVO> ITSrutasPlanUtilizacion(String horaFin, String horaIn, String nFecha,String[] pStops) throws Exception
	{
		return manager.ITSrutasPlanUtilizacion(horaFin, horaIn, nFecha, pStops);
	}

	//-------------------------PARTE B--------------------------//

	public static LinkedList<RoutesVO>ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha)throws Exception{
		return manager.ITSrutasPorEmpresaParadas(nombreEmpresa, fecha);
	}

	public static LinkedList<TripsVO>ITSviajesRetrasoTotalRuta(String idRuta, String fecha)throws Exception{
		return manager.ITSviajesRetrasoTotalRuta(idRuta, fecha);
	}
	
	public static LinkedList<String> ITSretardoHoraRuta (String idRuta, String fecha )throws Exception{
		return manager.ITSretardoHoraRuta(idRuta, fecha);
	}
	
	public static LinkedList<TripsVO> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaIn, String horaFin){
		return manager.ITSbuscarViajesParadas(idOrigen, idDestino, fecha, horaIn, horaFin);
	}
	
	public static RoutesVO ITSrutaMenorRetardo(String fecha)throws Exception{
		return manager.ITSrutaMenorRetardo(fecha);
	}
	
	
	//-------------------------PARTE C--------------------------//
	public static void ITScargarGTFS( ){
		manager.ITScargarGTFS();
	}
	
	public static LinkedList<ServiceVO> ITSserviciosMayorDistancia (String fecha){
		return manager.ITSserviciosMayorDistancia(fecha);
	}
	public static LinkedList<Integer> ITSretardosTrip(Integer nFecha, Integer nId)
	{
		return manager.ITSretardosTrip( nFecha, nId);
	}
	public static IList<Integer> ITSparadasCompartidas(String nFecha){
		return manager.ITSparadasCompartidas(nFecha);
	}

	
}
