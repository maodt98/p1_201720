package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.IList;
import model.data_structures.LinkedList;
import model.data_structures.MyInteger;
import model.data_structures.Node;
import model.logic.STSManager;
import model.vo.RoutesVO;
import model.vo.ServiceVO;
import model.vo.Stop_timesVO;
import model.vo.StopsVO;
import model.vo.TripsVO;

public class STSManagerView {
	//cambio
	private static STSManager m;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();

			int option = sc.nextInt();

			switch(option){
			case 1:
				Controller.ITScargarGTFS();
				break;
			case 2:
				System.out.println("Ingrese la fecha");
				String fecha2 = sc.next();
				Controller.ITScargarTR(fecha2);
				break;
				//1A	
			case 3:

				//Nombre empresa
				System.out.println("Ingrese el nombre de la empresa");
				String empresaCase3 = sc.next();

				//Fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase3 = sc.next();
				Integer fecha = Integer.parseInt(fechaCase3);

				//Rutas por empresa
				try{
				IList<RoutesVO> rutasPorEmpresa = Controller.ITSrutasPorEmpresa(fecha,empresaCase3);
				System.out.println("Las rutas que son prestadas por la empresa " + empresaCase3 + " en la fecha "+ fechaCase3 +" son:\n");
				Node<RoutesVO> currRoute = rutasPorEmpresa.getHead();
				while(currRoute!=null)
				{
					RoutesVO rutaCase3 = currRoute.getData();
					System.out.println("Id: " + rutaCase3.getRoute_id() + " | Short name: " + rutaCase3.getRoute_short_name());
					currRoute = currRoute.getNext();
				}
				System.out.println("\n");
				}
				catch(Exception e){
					e.printStackTrace();
					e.getMessage();
				}

				break;

				//2A	
			case 4:

				//id de la ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRouteCase4 = sc.next();

				//fecha 
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase4 = sc.next();

				//Viajes retrasados
				try{
				IList<TripsVO> listaCase4 = Controller.ITSviajesRetrasadosRuta(Integer.parseInt(idRouteCase4), Integer.parseInt(fechaCase4));
				System.out.println("Los viajes que estan retrasados en la ruta con id " + idRouteCase4 + " en la fecha " + fechaCase4 + "son:\n");
				Node<TripsVO> trip = listaCase4.getHead();
				while(trip!=null)
				{
					System.out.println("Trip id: " + trip.getData().getTrip_id());
					trip = trip.getNext();
				}
				System.out.println("\n");}
				catch(Exception e){
					e.getMessage();
					e.printStackTrace();
				}
				break;

				//3A	
			case 5:

				//fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase5 = sc.next();

				//paradas retrasadas
				try{
				LinkedList<StopsVO> listaCase5 = Controller.ITSparadasRetrasadasFecha(Integer.parseInt(fechaCase5));
				System.out.println("Las paradas en las que hubo retrasos en la fecha " + fechaCase5 + "son:\n");
				Node<StopsVO> stop = listaCase5.getHead();
				while(stop!=null)
				{

					StopsVO stopob = stop.getData();
					Integer inc = stopob.darVecesRetardado();
					System.out.println("Stop id:" + stopob.getStop_id() + "| Numero de incidentes: " + inc);
					stop = stop.getNext();
				}
				System.out.println("\n");}
				catch(Exception e){
					e.getMessage();
					e.printStackTrace();
				}
				break;

				//4A	
			case 6:

				//Id ruta
				System.out.println("Ingrese el id de la ruta deseada");
				String idRutaCase6 = sc.next();

				//fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase6 = sc.next();

				//Transbordos
				IList<Stop_timesVO> listaCase6;
				try {
					listaCase6 = Controller.ITStransbordosRuta(Integer.parseInt(fechaCase6), Integer.parseInt(idRutaCase6));
					System.out.println("Hay " + listaCase6.getSize() + " posibles transbordos:" + "para la ruta con id " + idRutaCase6 + " en la fecha " + fechaCase6 + "son:\n");
					int contadorCase6 = 1;
					Node<Stop_timesVO> currStops = listaCase6.getHead();
					while(currStops.getNext()!=null)
					{

						Stop_timesVO paradas = currStops.getData();
						System.out.println("\n" + contadorCase6 + ". Tiempo en transbordo: " +  Controller.ITStransbordosRuta(Integer.parseInt(fechaCase6), Integer.parseInt(idRutaCase6)));
						System.out.println("Paradas asociadas a transbordo:");

					}
					System.out.println("\n");
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				break;

				//5A	
				/**case 7:

				//Secuencia de paradas
				System.out.println("Ingrese los ID's de paradas Ej: 643-381-1274 (separelos con guiones)");
				String idsCase7 = sc.next();
				String[] splited = idsCase7.split("-");

				// TODO inicialize y a�ada los id�s a la lista de secuencia de paradas
				IList<String> secuenciaDeParadas = null;

				//Fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (A�oMesD�a)");
				String fechaCase7 = sc.next();

				//String hora inicio
				System.out.println("Ingrese la hora de inicio");
				String horaInicioCase7 = sc.next();

				//String hora fin 
				System.out.println("Ingrese la hora de finalizaci�n");
				String horaFinCase7 = sc.next();

				VOPlan plan = Controller.ITSrutasPlanUtilizacion(secuenciaDeParadas,fechaCase7, horaInicioCase7, horaFinCase7);
				System.out.println("Las paradas en la fecha y rango dadas son: \n");
				for (VOPlan.ParadaPlanVO parada : plan.getSecuenciaDeParadas()) 
				{
					//Parada
					System.out.println("Id parada:" + parada.getIdParada());
					System.out.println("Las rutas que utilizan esta parada son:");
					for (VOPlan.ParadaPlanVO.RutaPlanVO ruta: parada.getRutasAsociadasAParada()) 
					{
						//Ruta en parada
						System.out.println(   "- Ruta id:" + ruta.getIdRuta());
						System.out.println(   "Los viajes en esta ruta asociados a la parada son:");
						for (VOPlan.ParadaPlanVO.RutaPlanVO.ViajePlanVO viaje : ruta.getViajesEnRuta())
						{
							//viaje en ruta 
							System.out.println("     -" + viaje.getIdViaje() + "| Hora de parada :" + viaje.getHoraDeParada());
						}
					}
				}
				break;		**/

			case 8:
				System.out.println("Ingrese el nombre de la empresa");
				String empresa = sc.nextLine();
				empresa = sc.nextLine();
				System.out.println("Ingrese la fecha");
				String fecha8 = sc.next();
				try {
					LinkedList<RoutesVO> rutas = Controller.ITSrutasPorEmpresaParadas(empresa, fecha8);
					Node<RoutesVO> rutaCase8 = rutas.getHead();
					while(rutaCase8 != null){
						System.out.println("- Id de la ruta: "+rutaCase8.getData().getRoute_id());
						System.out.println("- Nombre de la ruta: "+rutaCase8.getData().getRoute_short_name());
						rutaCase8 = rutaCase8.getNext();
					}
				}
				catch(Exception e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
				break;
			case 9:
				System.out.println("Ingrese el id de la ruta");
				String idRuta9 = sc.nextLine();
				idRuta9 = sc.nextLine();
				System.out.println("Ingrese la fecha");
				String fecha9 = sc.next();
				try {
					LinkedList<TripsVO> tripsCase9 = Controller.ITSviajesRetrasoTotalRuta(idRuta9, fecha9);
					System.out.println("Existen "+tripsCase9.getSize()+" rutas que cumplen las condiciones dadas.");
					Node<TripsVO> tripCase9 = tripsCase9.getHead();
					while(tripCase9 != null){
						System.out.println("Id del viaje: "+tripCase9.getData().getTrip_id());
						tripCase9 = tripCase9.getNext();
					}
				}
				catch(Exception e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
				break;
			case 10:
				System.out.println("Ingrese el id de la ruta");
				String idRuta10 = sc.nextLine();
				idRuta10 = sc.nextLine();
				System.out.println("Ingrese la fecha");
				String fecha10 = sc.next();
				try {
					LinkedList<String> rangosCase10 = Controller.ITSretardoHoraRuta(idRuta10, fecha10);
					System.out.println("InformaciÃ³n del rango:");
					System.out.println("Hora inicial: "+rangosCase10.getHead().getData());
					System.out.println("Hora final: "+rangosCase10.getHead().getNext().getData());
					System.out.println("Numero de retardos: "+rangosCase10.getHead().getNext().getNext().getData());
				}
				catch(Exception e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
				break;
			case 11:
				//Id ruta origen
				System.out.println("Ingrese el id de la parada origen");
				String idOrigen11 = sc.next();
				//Id ruta destino
				System.out.println("Ingrese el id de la parada destino");
				String idDestino11 = sc.next();
				//Fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (AñoMesDía)");
				String fechaCase11 = sc.next();
				//Hora de inicio
				System.out.println("Ingrese la hora inicial");
				String horaInicio11 = sc.next();
				//Hora de inicio
				System.out.println("Ingrese la hora final");
				String horaFin11 = sc.next();

				IList<TripsVO> listaViajes11=Controller.ITSbuscarViajesParadas(idOrigen11, idDestino11, fechaCase11, horaInicio11, horaFin11);
				System.out.println("Existen "+listaViajes11.getSize()+" rangos que cumplen las condiciones dadas.");
				Node<TripsVO> actualCase11 = listaViajes11.getHead();
				while(actualCase11 != null){
					System.out.println("Id del viaje: "+actualCase11.getData().getTrip_id());
					actualCase11 = actualCase11.getNext();
				}

				break;

			case 12:				
				//fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (AñoMesDía)");
				String fechaCase12 = sc.next();

				try{
					RoutesVO ruta12=Controller.ITSrutaMenorRetardo(fechaCase12);
					System.out.println("Id de la ruta: "+ruta12.getRoute_id());
					System.out.println("Nombre corto de la ruta: "+ruta12.getRoute_short_name());
					System.out.println("Retardo promedio de la ruta: ");
				}
				catch(Exception e){
					e.getMessage();
					e.printStackTrace();
				}

				break;		

			case 13:

				//fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (AÃ±oMesDÃ­a)");
				String fechaCase13 = sc.next();

				IList<ServiceVO> listaCase13 = Controller.ITSserviciosMayorDistancia(fechaCase13);
				System.out.println("Los servicios que mas distancia corren en la fecha " + fechaCase13 +" son:\n");
				Node<ServiceVO> actualCase13 = listaCase13.getHead();
				while(actualCase13 != null){
					System.out.println("Id del servicio: " + actualCase13.getData().getServiceId() + "| Distancia recorrida: " + actualCase13.getData().getDistanciaRecorrida());
					actualCase13 = actualCase13.getNext();
				}
				break;
				//3C	
			case 14:
				//Id viaje
				System.out.println("Ingrese el id del viaje deseado");
				String idViajeCase14 = sc.next();

				//fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (Aï¿½oMesDï¿½a)");
				String fechaCase14 = sc.next();

				IList<Integer> listaRetardos14 = Controller.ITSretardosTrip(Integer.parseInt(fechaCase14), Integer.parseInt(idViajeCase14));
				System.out.println("Existen "+listaRetardos14.getSize()+" rangos que cumplen las condiciones dadas.");
				Node<Integer> actCase14 = listaRetardos14.getHead();
				while(actCase14 != null){
					System.out.println(actCase14.getData().toString());
					actCase14 = actCase14.getNext();
				}
				break;
			case 15:
				//fecha
				System.out.println("Ingrese la fecha deseada Ej: 20170625 (AñoMesDía)");
				String fechaCase15 = sc.next();

				IList<Integer> listaParadas15=Controller.ITSparadasCompartidas(fechaCase15);
				System.out.println("Existen "+listaParadas15.getSize()+" paradas que cumplen las condiciones dadas.");
				Node<Integer> actCase15  = listaParadas15.getHead();
				while(actCase15 != null)
				{
					System.out.println("Id de la parada: "+actCase15.getData());
					//System.out.println("Numero de incidentes: "+parada15.getNumeroIncidentes());
					actCase15 = actCase15.getNext();
				}
				break;
			case 16:	
				fin=true;
				sc.close();
				break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 1----------------------");
		System.out.println("Cargar data (1C):");
		System.out.println("1. Cargar la informaciÃ³n estÃ¡tica necesaria para la operaciÃ³n del sistema");
		System.out.println("2. Cargar la informaciÃ³n en tiempo real de los buses dada una fecha\n");

		System.out.println("Parte A:");
		System.out.println("3.Obtenga una lista de rutas ordenada por id, que son ofrecidas por una empresa determinada en una fecha dada. (1A)");
		System.out.println("4.Dada una ruta y una fecha, obtenga los viajes retrasados ordenados por id. (2A)");
		System.out.println("5.Obtenga una lista de Paradas ordenada por el numero de total incidentes, en las que hubo un retraso en una fecha dada. (3A)");
		System.out.println("6.Obtenga una lista ordenada (por tiempo total de viaje) con todos los transbordos posibles y las paradas asociadas a dichos transbordos para una determinada ruta en una fecha dada(4A)");
		System.out.println("7.Obtenga una lista en la que en cada posiciÃ³n se tiene una lista con la informaciÃ³n de una parada especÃ­fica. "
				+ "\n  A su vez en cada posiciÃ³n de esta lista, se tiene una lista con los viajes de la parada que se estÃ¡ analizando. (5A)\n");

		System.out.println("Parte B: ");
		System.out.println("8. Dadas una empresa y una fecha, obtenga una lista ordenada (por cantidad de paradas) con las rutas cubiertas que cubre la empresa en la fecha.(1B)");
		System.out.println("9. Dada una ruta y una flecha, obtenga una lista ordenada (por id de viaje y localizaciÃ³n de las paradas de mayor a menor tiempo de retardo)\n  de los viajes que despuÃ©s de un retardo tuvieron retardo en todas las siguientes paradas. (2B)");
		System.out.println("10. Dada una ruta y una fecha dada, obtenga una lista ordenada (por tiempo total de retardo) de horas de mÃ¡s retardos (3B)");
		System.out.println("11. Dados el id de una parada de origen y una parada de destino, una fecha y franja de horario, obtener los viajes de bus que llegan de la parada origen a la destino sin transbordos en la fecha y horas dadas (4B)");
		System.out.println("12. Obtener la ruta que tiene el menor retardo promedio en una fecha dada (5B)\n");


		System.out.println("Parte C:");
		System.out.println("13. Obtenga una lista con los servicios en una fecha ordenados por la distancia que recorren. (2C)");
		System.out.println("14. Obtenga una lista con todos los retardos de un viaje en una fecha dada. (3C)");
		System.out.println("15. Obtenga una lista con todas las paradas del sistema que son compartidas por mas de una ruta en una fecha determinada. (4C)\n ");
		System.out.println("16. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}
}
